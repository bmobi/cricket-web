<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'HomeController@index');

Route::get('/getTeam', 'HomeController@getTeam');
Route::get('/getStatus', 'HomeController@getStatus');
Route::post('/setStatus', 'HomeController@setStatus');
Route::post('/updateStatus', 'HomeController@updateStatus');

Route::group(['prefix' => 'api'], function () {
    Route::get('recent', 'CricketController@getRecentMatches');
    Route::get('upcoming', 'CricketController@getUpcomingMatches');
	Route::get('live', 'CricketController@getLiveMatches');
	Route::get('news', 'CricketController@getNews');
	Route::get('image', 'CricketController@getImage');
});

Route::group(['prefix' => 'live-api'], function () {
    Route::get('live', 'CricketController@updateLiveMatches');
    Route::get('recent', 'CricketController@updateRecentMatches');
});

Auth::routes();
