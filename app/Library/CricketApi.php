<?php

namespace App\Library;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Promise;
use GuzzleHttp\Exception\RequestException;

use Illuminate\Support\Facades\Cache;

class CricketApi
{
	protected $pathPrefix;
	protected $guzzleObject;
	protected $apiConfigs;
	protected $apiUrls;
	protected $accessToken;
	public $returnData = [];

	public function __construct() {
		$this->apiConfig = \Config::get('cricket.api');
		$this->apiUrls = \Config::get('cricket.urls');

		$this->guzzleObject = new Client([
			'base_uri' => $this->apiConfig['base_uri'],// Base URI is used with relative requests
			'timeout'  => 30.0,// You can set any number of default request options.
		]);

		$this->accessToken = $this->get_valid_token();
	}


	/**
	* Get valid token
	*
	* This function will get the provide valid token.
	* This access token will be automatically passed to getData() to get the response.
	*
	*/
	function get_valid_token()
	{
		//Our device ID
		$deviceCheck = $this->apiConfigs['device_id'];
		
		//Get previously saved token info
		$oldTokenInfo = Cache::get( 'auth' );
		
		/**
		 * Check whether the token is expired or empty
		 */
		if ( $oldTokenInfo and !empty( $oldTokenInfo[$deviceCheck]['expires'] ) and time() < ( $oldTokenInfo[$deviceCheck]['expires'] ) ) {
			return $oldTokenInfo[$deviceCheck]['token'];
		} else {
			//Call
			$callResponse = $this->make_request(
				$this->apiUrls['auth']['callUri'],
				$this->apiConfig,
				'POST'
			);

			//If we have token?
			if ( !empty ( $callResponse['auth']['access_token'] ) ) {
				
				//Store access token & expire time to a file.
				$oldTokenInfo[$deviceCheck] = array(
					'token' => $callResponse['auth']['access_token'],
					'expires' => $callResponse['auth']['expires'],
					'deviceID' => $deviceCheck,
				);
				
				$expiresAt = \Carbon\Carbon::now()->addMinutes(60); 
				//Save this in cache
				if (Cache::has('auth')) {
					Cache::put( 'auth', $oldTokenInfo, $expiresAt);
				} else {
					Cache::add( 'auth', $oldTokenInfo, $expiresAt);
				}
				
				//Set access token variable
				return $callResponse['auth']['access_token'];
				
			} else {
				$this->returnData['message'] = "Unable to get token";
				$this->returnData['status'] = 412;
				die(json_encode( $this->returnData ));
			}
		}
			
	}

	/**
	* Make requests
	* 
	*/
    public function make_request ( $uri, $params = array (), $method = 'GET', $continue = false )
    {
		//Final response
		$finalResponse = $this->returnData;
		$failure = true;
		
		//If it's get request?
		$queryString = '';
		if ( $method == 'GET' and count( $params ) > 0 ) {
			$queryString = '?' . http_build_query( $params );
			$params = array();
		}

		//Try
		try {
			//Making a request
			$response = $this->guzzleObject->request( $method, $uri . '/' . $queryString, [
				'http_errors' => false,
				'connect_timeout' => 30.00,
				'form_params' => $params,
			] );

			//Response body
			$body = json_decode( $response->getBody(), true );

			//If it's 200
			if ( !empty( $body['status_code'] ) and $body['status_code'] == 200 ) {
				return $body;
			} else {
				$finalResponse['message'] = ( !empty( $body['status_msg'] ) ? ' ' . $body['status_msg'] : '' );
				$finalResponse['status'] = 401;
			}
		
		//Catch exception!
		} catch (RequestException $e) {
			$finalResponse['message'] = $e->getMessage();
			$finalResponse['status'] = 406;
		}
		
		//Throw response if it's not success till now!
		if ( $failure and !$continue ) {
			die(json_encode( $finalResponse ));
		}
    }

    
	/**
	* Get current matches
	* 
	*/
    public function getRecentMatches ()
    {
		//Get data from cache
		$allMatches = Cache::get( $this->apiUrls['recent_matches']['url'] );
		return !empty( $allMatches['recent'] ) ? $allMatches['recent'] : [];
	}
	

	/**
	* Get upcoming matches
	* 
	*/
    public function getUpcomingMatches ()
    {
		//Get data from cache
		$allMatches = Cache::get( $this->apiUrls['recent_matches']['url'] );
		return !empty( $allMatches['upcoming'] ) ? $allMatches['upcoming'] : [];
	}


	/**
	* Get live matches
	* 
	*/
    public function getLiveMatches ()
    {
		//Get data from cache
		$allMatches = Cache::get($this->apiUrls['live_matches']['url'] );
		return !empty( $allMatches ) ? $allMatches : [];
	}

	/**
	* Get news
	* 
	*/
    public function getNewsBkp ()
    {
		//Fet contents
		$news = [
			[
				'author' => 'Cricketcountry',
				'link' => 'http://cricket.yahoo.com/news/ipl-2017-virat-kohli-needs-043746939.html',
				'publishedDate' => '2017-04-18T04:37:46-00:00',
				'title' => "IPL 2017: Virat Kohli needs to play Chris Gayle in Royal Challengers Bangalore (RCB) XI, says Sourav Ganguly",
				'description' => 'In the previous match of RCB, skipper Kohli dropped his star opener Chris Gayle, who is only three runs away from becoming the first man to touch 10,000-run mark in T20 cricket.',
				'thumbUrl' => 'https://s.yimg.com/lo/api/res/1.2/eDz9IoiFpW.Zwmnoz1y2kA--/YXBwaWQ9Y3J0O2ZpPWZpdDtoPTc1O3c9MTAw/http://media.zenfs.com/en-IN/homerun/cricketcountry_577/907fbbf2eb53f63ce6a8a61e50cd4637',
			],
			[
				'author' => 'Cricketcountry',
				'link' => 'http://cricket.yahoo.com/news/ipl-2017-why-manish-pandey-042748723.html',
				'publishedDate' => '2017-04-18T04:27:48-00:00',
				'title' => "IPL 2017: Why Manish Pandey is the among the smartest finishers in limited-overs",
				'description' => 'There is nothing chulbul (mischievous) about KKR’s Manish Pandey. Rather, there is an infectious calmness. There is also the rare swagger...',
				'thumbUrl' => 'https://s.yimg.com/lo/api/res/1.2/Uy9GJbC2MWpwyoH6L3ReXw--/YXBwaWQ9Y3J0O2ZpPWZpdDtoPTc1O3c9MTAw/http://media.zenfs.com/en-IN/homerun/cricketcountry_577/ec6b76f864844df59e35363b6780ea41',
			],
			[
				'author' => 'Cricketcountry',
				'link' => 'http://cricket.yahoo.com/news/ipl-2017-manan-vohra-laments-041232882.html',
				'publishedDate' => '2017-04-18T04:12:32-00:00',
				'title' => "IPL 2017: Manan Vohra laments opportunity to finish game against Sunrisers Hyderabad (SRH)",
				'description' => 'Kings XI Punjab (KXIP) might have lost a winning game by mere 5 runs in Match 19 of IPL 2017, but one man played a lone battle',
				'thumbUrl' => 'https://s.yimg.com/lo/api/res/1.2/EEobYrVhZA6WvEQRvErHZA--/YXBwaWQ9Y3J0O2ZpPWZpdDtoPTc1O3c9MTAw/http://media.zenfs.com/en-IN/homerun/cricketcountry_577/fa17b04fd3a1b40839c6277c5cfd91c9',
			],
			[
				'author' => 'ANI',
				'link' => 'http://cricket.yahoo.com/news/windies-recall-kieran-powell-first-test-against-pakistan-035501382.html',
				'publishedDate' => '2017-04-18T03:55:01-00:00',
				'title' => "Windies recall Kieran Powell for first Test against Pakistan",
				'description' => 'Barbados [West Indies], Apr 18 (ANI): West Indies batsman Kieran Powell has been recalled into the Test squad for the first match of the three-Test series against Pakistan beginning April 22 at Barbados.  \"We are happy to be able to give a recall to Kieran Powell for one of the opening slots, although he did not have the best regional first-class season,\" ESPNcrincinfo quoted West Indies chairman of selectors Courtney Browne as saying.',
				'thumbUrl' => '',
			],
			[
				'author' => 'CRICKETNMORE.com',
				'link' => 'http://cricket.yahoo.com/news/bhuvneshwar-kumar-joins-elite-club-033508552.html',
				'publishedDate' => '2017-04-18T03:35:08-00:00',
				'title' => "Bhuvneshwar Kumar joins elite club",
				'description' => 'April 18 (CRICKETNMORE - Sunrisers Hyderabad fast bowler Bhuvneshwar Kumar&apos;s magical performance with the ball (5/19) helped the team to beat Kings XI Punjab in a close match on Monday. He also joins the elite club of bowlers who have taken 100 wickets in IPL.',
				'thumbUrl' => 'https://s.yimg.com/lo/api/res/1.2/3yQWx8mTnY8DIj1MrLafrA--/YXBwaWQ9Y3J0O2ZpPWZpdDtoPTc1O3c9MTAw/http://media.zenfs.com/en-IN/homerun/cricketnmore_com_482/7cb6eea892cafc4ab152cdd430f80035',
			],
		];
		
		return $news;
	}

	/**
	* Get news
	* 
	*/
    public function getNews ()
    {
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://query.yahooapis.com/v1/public/yql");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,
		            "q=select%20*%20from%20cricket.news%20%20where%20region%3D%22in%22&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0");

		// in real life you should use something like:
		// curl_setopt($ch, CURLOPT_POSTFIELDS, 
		//          http_build_query(array('postvar1' => 'value1')));

		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);

		curl_close ($ch);
 		$server_output = json_decode($server_output); 

		// further processing ....
		if(property_exists($server_output, 'query')) { 
			return ($server_output->query->results->item); 
		} else { 
			die("false");
		}
	}

	public function appendTeamImage($matchTeams){
		$basePath = 'http://cricket.apphosthub.com/cric-app/public/images/teams/';
		$defaultImage = $basePath . 'default.png';
		foreach($matchTeams as $teamIndex => $teams){
			$teamImage = $basePath . $teams['key'] . '.png';
			$matchTeams[$teamIndex]['logo'] = @file_exists( getcwd() . '/public/images/teams/' . $teams['key'] . '.png' ) ? $teamImage : $defaultImage;
		}
		return $matchTeams;
	}


	/**
	* Put live matches
	* 
	*/
    public function updateLiveMatches()
    {
		//Set params
		$params = array(
			'access_token' => $this->accessToken,
			'card_type' => 'summary_card',//Optional
		);
		
		//Call
		$callResponse = $this->make_request(
			$this->apiUrls['live_matches']['callUri'],
			$params
		);
		
		//If we have token?
		$todayTime = strtotime( 'today' );
		if ( !empty ( $callResponse['data']['cards'] ) ) {
			
			//Prep data
			$allMatches = array();
			
			foreach ( $callResponse['data']['cards'] as $match ) {
				
				//Set time
				$match['start_date']['iso'] = !empty ( $match['start_date']['iso'] ) ? strtotime( $match['start_date']['iso'] ) : time();

				//If it's today's match?
				if ( $match['start_date']['iso'] < $todayTime OR $match['start_date']['iso'] > ( $todayTime + 86400 ) ) {
					continue;
				}
				
				//Prep match array!
				$allMatches[] = array(
					
					//Names
					'basics_info' => array(
						'status' => $match['status'],
						'venue' => $match['venue'],
						'ref' => $match['ref'],
						'start_date' => $match['start_date']['iso'],
						'description' => !empty ( $match['description'] ) ? $match['description'] : false,
						'match_overs' => !empty ( $match['match_overs'] ) ? $match['match_overs'] : false,
						'winner_team' => !empty ( $match['winner_team'] ) ? $match['winner_team'] : false,
					),
					'names' => array(
						'related_name' => $match['related_name'],
						'name' => $match['name'],
						'short_name' => $match['short_name'],
						'title' => $match['title'],
					),
					'season' => $match['season'],
					'teams' => $this->appendTeamImage($match['teams']),
					'others' => array(
						'format' => $match['format'],
						'key' => $match['key'],
					),
					'msgs' => $match['msgs'],
					'now' => $match['now'],
				);
			}

			$expiresAt = \Carbon\Carbon::now()->addMinutes(1440); 
			//Save this in cache
			Cache::put('live_matches', $allMatches, $expiresAt);
			
			//Return
			return "Live Matches Updated";
			
		} else {
			$this->returnData['message'] = 'global_error_unable_to_update_content';
			$this->returnData['status'] = 405;
			return $this->returnData;
		}
	}

	/**
	* Put recent matches
	* 
	*/
    public function updateRecentMatches()
    {

    	//Set params
		$params = array(
			'access_token' => $this->accessToken,
			//'card_type' => 'summary_card',//Optional
		);
		
		//Call
		$callResponse = $this->make_request(
			$this->apiUrls['recent_matches']['callUri'],
			$params
		);
		
		//If we have token?
		if ( !empty ( $callResponse['data']['months'] ) ) {
			
			//Set match params
			$matchParams = array(
				'access_token' => $this->accessToken,
				//'card_type' => 'full_card',//Optional
			);
			
			//Prep data
			$allMatches = array();
			$currentDay = date('d'); //Current day
			foreach ( $callResponse['data']['months'][0]['days'] as $dayData ) {
				
				//Iterate all day matches!
				$day = $dayData['day'];
				$dayMatches = array();

				//Day key?
				if ( $day < $currentDay ) {
					$dayStatusKey = 'recent';
				} elseif ( $day == $currentDay ) {
					$dayStatusKey = 'live';
				} else {
					$dayStatusKey = 'upcoming';
				}

				if ( count( $dayData['matches'] ) and !empty( $dayData['matches'][0]['status'] ) ) {
					foreach ( $dayData['matches'] as $match ) {
						
						$matchResult = (object) array();
						if ( $dayStatusKey == 'recent' ) {
							
							//Fetch match result
							$matchResult = $this->getMatchSummary( $match['key'] );
							
							//If result was not successful?
							if ( !$matchResult ) {
								continue;//Skip this match!
							}
						}

						//Set time
						$match['start_date']['iso'] = !empty ( $match['start_date']['iso'] ) ? strtotime( $match['start_date']['iso'] ) : time();

						//Prep match array!
						$dayMatches[] = array(
							
							//Names
							'basics_info' => array(
								'status' => $match['status'],
								'venue' => $match['venue'],
								'ref' => $match['ref'],
								'start_date' => $match['start_date']['iso'],
								'winner_team' => !empty ( $match['winner_team'] ) ? $match['winner_team'] : false,
							),
							'names' => array(
								'related_name' => $match['related_name'],
								'name' => $match['name'],
								'short_name' => $match['short_name'],
								'title' => $match['title'],
							),
							'season' => $match['season'],
							'teams' => $this->appendTeamImage($match['teams']),
							'others' => array(
								'format' => $match['format'],
								'key' => $match['key'],
							),
							'results' => $matchResult,
							'msgs' => $match['msgs'],
						);
					}
				}

				//Add data to $allMatches
				if ( count( $dayMatches ) > 0 ) {
					$allMatches[$dayStatusKey][] = array(
						'day' => $day,
						'date' => $dayMatches[0]['basics_info']['start_date'],
						'matches' => $dayMatches,
					);
				}
			}

			$expiresAt = \Carbon\Carbon::now()->addMinutes(1440); 
			Cache::put('recent_matches', ['recent' => ( isset($allMatches['recent']) ? $allMatches['recent'] : []), 'upcoming' => $allMatches['upcoming']], $expiresAt);
			
			//Return
			return "Recent Match Updated";
			
		} else {
			$this->returnData['message'] = 'Unable to update content';
			$this->returnData['status'] = 405;
			return $this->returnData;
		}
	}

	/**
	* Get summary_card of a match!
	* 
	*/
    public function getMatchSummary ( $matchKey )
    {
		//Basics
		$cacheResponse = false;
		$summaryPath = $matchKey;
		$fullPath = $matchKey."_full";

		//Set match params
		$summaryRequestParams = array(
			'access_token' => $this->accessToken,
			'card_type' => 'summary_card',//Optional
		);
			
		$cacheResponse = Cache::get($summaryPath);
		
		//If cache data was found ? 
		if ( $cacheResponse ) {
			$summaryCallResponse = $cacheResponse;

		//Call this!
		} else {
			
			//Make another guzzle request!
			$summaryCallResponse = $this->make_request(
				'match/' . $matchKey,
				$summaryRequestParams,
				'GET',
				true
			);
		}
		
		//If we have full card and proper response of it?
		if ( !empty( $summaryCallResponse['data']['card_type'] )
			and $summaryCallResponse['data']['card_type'] == $summaryRequestParams['card_type']
			and !empty( $summaryCallResponse['data']['card']['winner_team'] )
		) {
			
			//Save it to cache
			if ( !$cacheResponse ) {
				$expiresAt = \Carbon\Carbon::now()->addMinutes(1440); 
				Cache::put( $summaryPath, $summaryCallResponse, $expiresAt);
				
				//Set match params
				$fullRequestParams = array(
					'access_token' => $this->accessToken,
					'card_type' => 'full_card',//Optional
				);
				
				//Also fetch full card!
				$fullCallResponse = $this->make_request(
					'match/' . $matchKey,
					$fullRequestParams,
					'GET',
					true
				);
				
				//Just save this one without any validations!
				Cache::put( $fullPath, $fullCallResponse, $expiresAt);
			}
			
			//Get innings score
			$teamA = $teamB = array(
				'runs' => 0,
				'balls' => 0,
				'overs' => 0,
				'wickets' => 0,
			);
			
			//Iterate all innings
			foreach ( $summaryCallResponse['data']['card']['innings'] as $key => $inning ) {
				
				//If it's team a?
				if ( strpos($key, 'a_') !== false ) {
					$teamA['runs'] += $inning['runs'];
					$teamA['balls'] += $inning['balls'];
					$teamA['overs'] += $inning['overs'];
					$teamA['wickets'] += $inning['wickets'];
					
				} else {
					$teamB['runs'] += $inning['runs'];
					$teamB['balls'] += $inning['balls'];
					$teamB['overs'] += $inning['overs'];
					$teamB['wickets'] += $inning['wickets'];
				}
			}
			
			//Return prepped array
			return array(
				'winner_team' => $summaryCallResponse['data']['card']['winner_team'],
				'innings' => array(
					'a' => $teamA,
					'b' => $teamB,
				),
			);
			
		} else {
			return false;
		}
	}
}