<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use J42\LaravelFirebase\LaravelFirebaseFacade as FirebaseApi;

class Match extends Model
{
   protected $connection = 'firebase';

   public static function getStatus() {
      $data = FirebaseApi::get('/match');
      return ($data->getContents());
   }

   public static function setStatus($request) {
   		$response = FirebaseApi::set('/match', $request->all());
        return ($response->getContents());
   }

   public static function setMatchParam($key, $value){
   		return FirebaseApi::set($key, $value);
   }
}
