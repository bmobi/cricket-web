<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team as Team;
use App\Match as Match;


class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        //$data = (\Firebase::set('/user/team', collect(['team1' => ['admin', 'user'], 'team2' => 'guest'])));
        //$data = \Firebase::push('/user/team', collect(['team3' => 'employee']));

        //$data = \Firebase::get('/match');
        //dd($data->getContents());
        //return view('home.index', ['dataSet' => json_decode($data->getContents())]);

        return view('home.index', ['teams' => Team::get()]);
    }

    public function getTeam(){
        die(Team::get());
    }

    public function getStatus(){  
        die(Match::getStatus());
    }

    public function setStatus(Request $request){    
        die(Match::setStatus($request));
    }

    public function updateStatus(Request $request){ 
        $postData = $request->all();

        foreach ($postData as $key => $value) {
            $response = Match::setMatchParam('/match/' . $key, $value);
        }

        die($response->getContents());
    }
}
