<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team as Team;

class Home extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
   		//$data = (\Firebase::set('/user/team', collect(['team1' => ['admin', 'user'], 'team2' => 'guest'])));
   		//$data = \Firebase::push('/user/team', collect(['team3' => 'employee']));

   		//$data = \Firebase::get('/user/role');
   		//print_r($data->getContents());
   		//return view('home.index', ['dataSet' => json_decode($data->getContents())]);

   		return view('home.index', ['teams' => Team::get()]);
	}

	public function getTeam(){
		die(Team::get());
	}

	public function getStatus(){	
		$data = \Firebase::get('/match');
		die($data->getContents());
	}

	public function setStatus(Request $request){	
		$response = \Firebase::set('/match', $request->all());
		die($response->getContents());
	}

	public function updateStatus(Request $request){	
		$postData = $request->all();

		foreach ($postData as $key => $value) {
			$response = \Firebase::set('/match/' . $key, $value);
		}

		die($response->getContents());
	}
}
