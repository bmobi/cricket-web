<?php

namespace App\Http\Controllers;
use App\Library as CricketLibrary;

class CricketController extends Controller
{
	protected $cricketApi;

	public function __construct() {
		$this->cricketApi = new CricketLibrary\CricketApi();
	}


	/**
	* Get current matches
	* 
	*/
    public function getRecentMatches ()
    {
		return $this->cricketApi->getRecentMatches();
	}
	

	/**
	* Get upcoming matches
	* 
	*/
    public function getUpcomingMatches ()
    {
		return $this->cricketApi->getUpcomingMatches();
	}


	/**
	* Get live matches
	* 
	*/
    public function getLiveMatches ()
    {
		return $this->cricketApi->getLiveMatches();
	}



	/**
	* Put live matches
	* 
	*/
    public function updateLiveMatches()
    {
		return $this->cricketApi->updateLiveMatches();
	}

	/**
	* Put recent matches
	* 
	*/
    public function updateRecentMatches()
    {

    	return $this->cricketApi->updateRecentMatches();
	}

	public function getNews()
	{
		return $this->cricketApi->getNews();
	} 


	// testing code
	public function getImage(){

		/*$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://query.yahooapis.com/v1/public/yql");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,
		            "q=select%20*%20from%20cricket.news%20%20where%20region%3D%22in%22&format=json&diagnostics=true&env=store%3A%2F%2F0TxIGQMQbObzvU4Apia0V0");

		// in real life you should use something like:
		// curl_setopt($ch, CURLOPT_POSTFIELDS, 
		//          http_build_query(array('postvar1' => 'value1')));

		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);

		curl_close ($ch);
 		$server_output = json_decode($server_output); 

		// further processing ....
		if(property_exists($server_output, 'query')) { 
			print_r(json_encode($server_output->query->results->item)); 
		} else { 
			dd("nothing found");
		}
		
		if(file_exists(getcwd() . "/public/images/teams/mi.png")){
			echo "<br/> file exist";
		}



		dd(@getimagesize('http://cricket.apphosthub.com/cric-app/public/images/teams/mi.png'));*/
	}

}