<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Library as CricketLib;

class UpdateRecentMatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'match:recent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Recent Matches';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $cricketApi = new CricketLib\CricketApi();
       return  $cricketApi->updateRecentMatches();
    }
}
