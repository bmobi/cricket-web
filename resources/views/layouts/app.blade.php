<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" ng-app="cricApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CricApp') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-theme.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
    <link href="{{ asset('plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/fullcalendar/fullcalendar.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/xcharts/xcharts.min.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/select2/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/justified-gallery/justifiedGallery.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style_v2.css') }}" rel="stylesheet">
    <link href="{{ asset('plugins/chartist/chartist.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/loading-bar.css') }}" rel="stylesheet">
    
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="screensaver">
        <canvas id="canvas"></canvas>
        <i class="fa fa-lock" id="screen_unlock"></i>
    </div>
    <div id="modalbox">
        <div class="devoops-modal">
            <div class="devoops-modal-header">
                <div class="modal-header-name">
                    <span>Cricket Application</span>
                </div>
                <div class="box-icons">
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="devoops-modal-inner">
            </div>
            <div class="devoops-modal-bottom">
            </div>
        </div>
    </div>
    <header class="navbar">
        <div class="container-fluid expanded-panel">
            <div class="row">
                <div id="logo" class="col-xs-12 col-sm-2">
                    <a href="{{ url('/') }}">Cricket Application</a>
                </div>
                <div id="top-panel" class="col-xs-12 col-sm-10">
                    <div class="row">
                        <div class="col-xs-8 col-sm-4">
                            
                        </div>
                        <div class="col-xs-4 col-sm-8 top-panel-right">
                            <div class="pull-right" id="logo">
                                @if (Auth::guest())
                                    <a href="{{ route('login') }}">Login</a>
                                @else
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--End Header-->
    <!--Start Container-->
    <div id="main" class="container-fluid">
        <div class="row">
            <!--Start Content-->
            <div id="content" class="col-xs-12 col-sm-12">
                 @yield('content')
            </div>
            <!--End Content-->
        </div>
    </div>

    
    <!-- Scripts -->
    <script type="text/javascript"  src="{{ asset('js/angular/angular.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/angular/angular-animate.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/angular/angular-aria.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/angular/angular-messages.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/angular/angular-route.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/ui-router/angular-ui-router.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/angular/angular-resource.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/angular/angular-cookies.min.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/angular/dirPagination.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/loading-bar.js') }}"></script>

    <script src="{{ asset('module/app.js') }}"></script>
    <script src="{{ asset('module/filters/filter.js') }}"></script>
    <script src="{{ asset('module/constants/constant.js') }}"></script>
    <script src="{{ asset('module/services/service.js') }}"></script>
     <script src="{{ asset('module/directives/directive.js') }}"></script>
    <script src="{{ asset('module/controllers/controller.js') }}"></script>
    <script src="{{ asset('module/config.js') }}"></script>

    <script src="{{ asset('js/jquery/jquery-2.1.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ asset('plugins/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/justified-gallery/jquery.justifiedGallery.min.js') }}"></script>
    <script src="{{ asset('plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('plugins/tinymce/jquery.tinymce.min.js') }}"></script>
    <!-- All functions for this theme + document.ready processing -->
    <script src="{{ asset('js/devoops.js') }}"></script>

    <script src="https://www.gstatic.com/firebasejs/3.7.6/firebase.js"></script>
        <script>
          // Initialize Firebase
          var config = {
            apiKey: "AIzaSyDlRUc5suaRUDxx8DyS5FIzVXJ2ngH_niI",
            authDomain: "test-app-cb78e.firebaseapp.com",
            databaseURL: "https://test-app-cb78e.firebaseio.com",
            projectId: "test-app-cb78e",
            storageBucket: "test-app-cb78e.appspot.com",
            messagingSenderId: "882130970566"
          };
          firebase.initializeApp(config);
        </script>
</body>
</html>
