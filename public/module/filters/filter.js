'use strict';

var cricFilters = angular.module('cricFilters', [])

    cricFilters
        .filter('isFavourate', function() {
            return function(team, favourateTeams) {
            	if(favourateTeams == "0") {
            		return false;
            	}
                return (favourateTeams.toUpperCase() == 'BOTH') ? true : ((favourateTeams.toUpperCase() == team.toUpperCase()) ? true : false);
            };
        })