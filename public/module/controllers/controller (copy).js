
	function HomeController($scope, cfpLoadingBar, $location, match, dataReader){
		$scope.secondTeam = $scope.firstTeam = [];  
		$scope.disableStartButton = true;

		$scope.match = match;

		$scope.inning = {
			'team1': null,
			'team2': null,
			'batting' : null,
			'bowling' : null
		}

		function filterFromTeam(filterArray, filterValue){
			var otherTeam = [];
			angular.forEach(filterArray, function(value, index){
				if(value.name != filterValue) {
					otherTeam.push(filterArray[index]);
				}
			});

			return otherTeam;
		};

		$scope.changeSelection = function(selectedTeam, selectedFor) {
			if($scope.inning.team1 && $scope.inning.team2) {
				$scope.inning[selectedFor] = selectedTeam;
				$scope.inning[(selectedFor == 'batting') ? 'bowling' : 'batting'] = (selectedTeam == 'team1') ? 'team2' : 'team1';
				$scope.disableStartButton = false;
			} else {
				$scope.disableStartButton = true;
				alert("Please select a teams first");
			}
		};

		$scope.startMatch = function(){
			$scope.match.battingTeam = $scope.inning[$scope.inning['batting']];
			$scope.match.bowlingTeam = $scope.inning[$scope.inning['bowling']];
			
			$scope.match.playing = $scope.match.battingTeam;
			dataReader.setStatus({
						'data' : $scope.match 
					})
					.then(function(response){
							$location.path('/match')
					});
		};

		$scope.$watch('inning.team1', function(newValue, oldValue){
			if(newValue) {
				$scope.secondTeam =  filterFromTeam($scope.allTeams, newValue);
			}
		});

		$scope.$watch('inning.team2', function(newValue, oldValue){
			if(newValue) {
				$scope.firstTeam = filterFromTeam($scope.allTeams, newValue);
			}
		});

		$scope.start = function() {
	      cfpLoadingBar.start();
	    };

	    $scope.complete = function () {
	      cfpLoadingBar.complete();
	    }

	    $scope.start();

		dataReader.getTeam()
				  .then(function(response){
					  	$scope.allTeams = response;
						angular.copy($scope.allTeams, $scope.firstTeam); 
						angular.copy($scope.allTeams, $scope.secondTeam); 
						$scope.complete();
				  })
	}

	function MatchController($scope, cfpLoadingBar, match, keywords, dataReader){
		console.log(keywords);
		$scope = angular.extend($scope, keywords);

		$scope.getRandomMessage = function(type){
			return $scope.messages[type][Math.floor(Math.random() * $scope.messages[type].length)];
		}

		$scope.hasBeenClicked = function(value){
			if($scope.lastBallStatus.length == 0)
				return false;

			return ($scope.lastBallStatus.indexOf(value) >= 0);
		}

		$scope.batsmanList = [];
		$scope.init = function(){
			$scope.match = match;

			$scope.wideBallFlag = false;
			$scope.noBallFlag = false;
			$scope.wicketFlag = false;
			$scope.balls = 0.0;
			$scope.selectedButton = "";
			$scope.onStrike = "";
			$scope.onBowling = "";
			$scope.batsmanList = [];
			$scope.bowlerList = [];
			$scope.lastBatsman = [];

			$scope.temp = {};
			$scope.message = "";
			$scope.lastBallStatus = [];
			$scope.lastBallHistory = [];
			$scope.batsman2Run = 0;
			$scope.batsman2Ball = 0;
			$scope.batsman1Run = 0;
			$scope.batsman1Ball = 0;
			$scope.batsman1 = "";
			$scope.batsman2 = "";
			$scope.disableFinishButton = false;
			$scope.boundaryFlag = false;
			$scope.showLoader = false;

		}

		$scope.initInning = function(response){
			var batsmanStatus = "";
			var runStatus = "";
			angular.forEach(response, function(value, index){
				$scope.match[index] = value;
			});
			$scope.balls = 0.0;
			$scope.match.score = 0;
			$scope.match.runs = Number(($scope.match.playing == $scope.match.battingTeam) ? $scope.match.battingScore : $scope.match.bowlingScore);
			$scope.balls  = (($scope.match.playing == $scope.match.battingTeam) ? $scope.match.battingOver : $scope.match.bowlingOver);
			


			if($scope.match.batsmanOneStatus) {
				batsmanStatus = $scope.match.batsmanOneStatus.split(" ");
				$scope.batsman1 = batsmanStatus[0];
				if(batsmanStatus.length == 2){
					runStatus = batsmanStatus[1].split("(");
					$scope.batsman1Run = Number(runStatus[0]);
					$scope.batsman1Ball = Number(runStatus[1].replace(")", ""));
				}

				if( !$scope.match.onStrike ){
					$scope.match.onStrike = batsmanStatus[0];
				}

				$scope.batsmanList.push({
					'name' : batsmanStatus[0],
					'run' : $scope.batsman1Run,
					'ball': $scope.batsman1Ball,
					'status': ($scope.match.onStrike.toUpperCase() == batsmanStatus[0].toUpperCase()) ? 'onStrike' : 'playing'
				})
			}

			if($scope.match.batsmanTwoStatus) {
				batsmanStatus = $scope.match.batsmanTwoStatus.split(" ");
				$scope.batsman2 = batsmanStatus[0];
				if(batsmanStatus.length == 2){
					runStatus = batsmanStatus[1].split("(");
					$scope.batsman2Run = Number(runStatus[0]);
					$scope.batsman2Ball = Number(runStatus[1].replace(")", ""));
				}

				if( !$scope.match.onStrike ){
					$scope.match.onStrike = batsmanStatus[0];
				}

				$scope.batsmanList.push({
					'name' : batsmanStatus[0],
					'run' : $scope.batsman2Run,
					'ball': $scope.batsman2Ball,
					'status': ($scope.match.onStrike.toUpperCase() == batsmanStatus[0].toUpperCase()) ? 'onStrike' : 'playing'
				})
			}

			if($scope.match.bowlerStatus && $scope.match.bowlerStatus != "0") {
				$scope.bowlerList.push({
					'name' : $scope.match.bowlerStatus,
					'run' : 10,
					'ball': 10,
					'status': 'bowling'
				});
				$scope.onBowling = $scope.match.bowlerStatus;
				$scope.bowler1 = $scope.match.bowlerStatus;
			}

			$scope.lastBallStatus = [];
			$scope.previousBall = [];
			if($scope.match.previousBall && $scope.match.previousBall != "0") {
				$scope.previousBall = $scope.match.previousBall.split(",");
			}
		}			  

		$scope.updateWicket = function(){

		}	

		$scope.updateOvers = function(){
			
		}	


		$scope.updateFavourite = function(team){
			$scope.showLoader = true;
			var favouriteTeam = "";

			if($scope.match.favouriteTeam)
				favouriteTeam = $scope.match.favouriteTeam.split(",");

			var param = {'favouriteTeam' : ''};
			if(favouriteTeam && favouriteTeam.length) {
				var indexOfTeam = favouriteTeam.indexOf(team);
				if(indexOfTeam == -1){
					$scope.match.favouriteTeam += (favouriteTeam[0]) ? ("," + team) : team;
				} else {
					favouriteTeam.splice(indexOfTeam, 1);
					$scope.match.favouriteTeam = favouriteTeam.join();
				}
			} else {
				$scope.match.favouriteTeam = team;
			}

			param['favouriteTeam'] = $scope.match.favouriteTeam;

			dataReader.updateStatus({
					'data' : param
			})
			.then(function(response){
				$scope.showLoader = false;
			})
		}	  

		$scope.substract = function(type){
			var modifieldValue = {};
			$scope.showLoader = true;
			if(type == 'score') {
				type = ($scope.match.playing == $scope.match.battingTeam) ? 'battingScore' : 'bowlingScore';
				$scope.match[type]--;
				modifieldValue[($scope.match.playing == $scope.match.battingTeam) ? 'battingScore' : 'bowlingScore'] = $scope.match[type];
			}

			if(type == 'wickets') {
				type = ($scope.match.playing == $scope.match.battingTeam) ? 'battingWickets' : 'bowlingWickets';

				if(Number($scope.match[type]) <= 0) {
					$scope.showLoader = false;
					return;
				} 

				$scope.match[type]--;
				modifieldValue[($scope.match.playing == $scope.match.battingTeam) ? 'battingWickets' : 'bowlingWickets'] = $scope.match[type];
			}

			if(type == 'balls'){
				type = ($scope.match.playing == $scope.match.battingTeam) ? 'battingOver' : 'bowlingOver';
				if(Number($scope.match[type]) <= 0){
					$scope.balls = $scope.match[type] = 0.0;
					modifieldValue[($scope.match.playing == $scope.match.battingTeam) ? 'battingOver' : 'bowlingOver'] = $scope.match[type];
				} else {
					if( (($scope.match[type] - Math.floor($scope.match[type]).toFixed(1)) * 10) == 0){
						$scope.balls = $scope.match[type] = parseFloat(parseFloat($scope.match[type]) - 0.5).toFixed(1);
					} else {
						$scope.balls = $scope.match[type] = parseFloat(parseFloat($scope.match[type]) - 0.1).toFixed(1);
					}

					if ($scope.balls == 0){
						$scope.balls = $scope.match[type] = 0.0;
					}
					modifieldValue[($scope.match.playing == $scope.match.battingTeam) ? 'battingOver' : 'bowlingOver'] = $scope.match[type];
				}
			}
			$scope.updateTeamStatus(modifieldValue);
			
		}

		$scope.add = function(type) {
			var modifieldValue = {};
			$scope.showLoader = true;
			if(type == 'score') {
				type = ($scope.match.playing == $scope.match.battingTeam) ? 'battingScore' : 'bowlingScore';
				$scope.match[type]++;
				modifieldValue[($scope.match.playing == $scope.match.battingTeam) ? 'battingScore' : 'bowlingScore'] = $scope.match[type];
			}

			if(type == 'wickets') {
				type = ($scope.match.playing == $scope.match.battingTeam) ? 'battingWickets' : 'bowlingWickets';

				if(Number($scope.match[type]) >= 10) {
					$scope.showLoader = false;
					return;
				}
				
				$scope.match[type]++;
				modifieldValue[($scope.match.playing == $scope.match.battingTeam) ? 'battingWickets' : 'bowlingWickets'] = $scope.match[type];
			}

			if(type == 'balls'){
				type = ($scope.match.playing == $scope.match.battingTeam) ? 'battingOver' : 'bowlingOver';
				
				$scope.balls = $scope.match[type] = parseFloat(parseFloat($scope.match[type]) + 0.1).toFixed(1);
				if( (($scope.match[type] - Math.floor($scope.match[type]).toFixed(1)) * 10) >= 6){
					$scope.balls = $scope.match[type] = parseFloat(parseFloat($scope.match[type]) + 0.4).toFixed(1);
				}
				modifieldValue[($scope.match.playing == $scope.match.battingTeam) ? 'battingOver' : 'bowlingOver'] = $scope.match[type];
			}

			$scope.updateTeamStatus(modifieldValue);
			
		}

		$scope.updateTeamStatus = function(modifieldValue){
			$scope.showLoader = true;
			modifieldValue = $scope.updateSession(modifieldValue);
			dataReader.updateStatus({
				'data' : modifieldValue
			})
			.then(function(response){
				$scope.showLoader = false;
			})
		}

		$scope.updateStatus = function(type){
			$scope.selectedButton = type;
			$scope.updateCurrentStaus(type);
		}

		
		$scope.updateCurrentStaus = function(type) {
			$scope.showLoader = true;
			$scope.match.currentStatus = type;
			dataReader.updateStatus({
				'data' : {
					'currentStatus' : type
				}
			})
			.then(function(response){
				$scope.showLoader = false;
			})
		}

		$scope.updateMarketRate = function() {
			$scope.showLoader = true;
			dataReader.updateStatus({
				'data' : {
					'markerRateOne' : $scope.match.markerRateOne,
					'markerRateTwo' : $scope.match.markerRateTwo
				}
			})
			.then(function(response){
				$scope.showLoader = false;
			})
		}

		$scope.updateSession = function(uploadParam){
			if($scope.match.sessionOver && $scope.match.sessionOver != "0"){
				$scope.showLoader = true;
				var currentOver = (match.playing == match.battingTeam) ? match.battingOver : match.bowlingOver;
				var currentRun = (match.playing == match.battingTeam) ? match.battingScore : match.bowlingScore;

				var overRequired = Math.floor($scope.match.sessionOver) - Math.floor(currentOver);

				var totalBalls = Math.floor(overRequired) * 6;

				var remainingBalls = Math.round( (parseFloat(currentOver).toFixed(1) - Math.floor(currentOver)) * 10 );
				if (totalBalls >= 0){
					if (remainingBalls > 0){
						totalBalls -= remainingBalls;
					} 
				}else {
					totalBalls = 0;
				}

				$scope.match.runXBallTwo = totalBalls;

				var runRequired = Number($scope.match.sessionStatusTwo) - Number(currentRun);
				if(runRequired > 0) {
					$scope.match.runXBallOne = runRequired;
				} else {
					$scope.match.runXBallOne = 0;
				}

				if(typeof uploadParam == 'undefined') {
					dataReader.updateStatus({
						'data' : {
							'sessionOver' 		: $scope.match.sessionOver,
							'sessionStatusOne' 	: $scope.match.sessionStatusOne,
							'sessionStatusTwo' 	: $scope.match.sessionStatusTwo,
							'runXBallOne' 		: $scope.match.runXBallOne,
							'runXBallTwo' 		: $scope.match.runXBallTwo
						}
					})
					.then(function(response){
						$scope.showLoader = false;
					});
				} else {
					return angular.extend(uploadParam, {
						'sessionOver' 		: $scope.match.sessionOver,
						'sessionStatusOne' 	: $scope.match.sessionStatusOne,
						'sessionStatusTwo' 	: $scope.match.sessionStatusTwo,
						'runXBallOne' 		: $scope.match.runXBallOne,
						'runXBallTwo' 		: $scope.match.runXBallTwo
					});
				}
			}

			return uploadParam;
		}

		$scope.updateRunXBall = function(){
			$scope.showLoader = true;
			dataReader.updateStatus({
				'data' : {
					'runXBallOne' : $scope.match.runXBallOne,
					'runXBallTwo' : $scope.match.runXBallTwo
				}
			})
			.then(function(response){
				$scope.showLoader = false;
			})
		}

		$scope.calculateRunXBall = function(){

			$scope.updateRunXBall();
		}

		$scope.finishInning = function(){
			$scope.showLoader = true;
			angular.copy($scope.match, $scope.temp); 

			$scope.temp.playing = ($scope.temp.battingTeam == $scope.temp.playing) ? $scope.temp.bowlingTeam : $scope.temp.battingTeam;
			$scope.temp.target  = ($scope.temp.battingTeam == $scope.temp.playing) ? $scope.temp.bowlingScore : $scope.temp.battingScore;
			$scope.temp.fallen  = $scope.temp.wickets;
			$scope.temp.runs   = $scope.temp.wickets = 0;
			$scope.temp.previousBall = 0;
			$scope.temp.onStrike = 0;
			$scope.temp.bowlerStatus = 0;
			$scope.temp.batsmanOneStatus = 0;
			$scope.temp.batsmanTwoStatus = 0;
			$scope.temp.ballsRemaining = 0;
			$scope.temp.currentStatus = "Break";

			++($scope.temp.inning);
			dataReader.setStatus({
					'data' : $scope.temp
				})
				.then(function(response){
					$scope.showLoader = false;
					if(response.inning <= 2){
						$scope.init();
						$scope.initInning(response);
					} else {
						$scope.disableFinishButton = true;
						$scope.message = "Match Finished";
					}
				});
		}

		$scope.updateLastBallStatus = function(){
			$scope.previousBall.splice(0, 1);
			$scope.match.previousBall = $scope.previousBall.join();
			/*dataReader.updateStatus({
				'data' : {
					'previousBalls' : $scope.match.previousBalls 
				}
			});*/
		}

		$scope.updateTvScreen = function(message){
			$scope.match.currentStatus = message;
			dataReader.updateStatus({
				'data' : {
					'currentStatus' : message
				}
			});
			
		}

		$scope.updateLiveMessage = function(message){
			$scope.match.localDescription = message;
			dataReader.updateStatus({
				'data' : {
					'localDescription' : message
				}
			});
		}

		$scope.addRun = function(run){
			$scope.activateUpdateButton = true;
			$scope.lastBallStatus.push(run);
			$scope.match.score = parseInt($scope.match.score) + parseInt(run);
			
			$scope.updateBatsmanStatus('run', run);
			$scope.updateBatsmanScore = false;
			
			if(parseInt(run) == 4 || parseInt(run) == 6){
				$scope.boundaryFlag = true;
				
			}
		}


		$scope.updateRun = function(runType, wicketType){
			wicketType = (typeof wicketType == 'undefined') ? "" : wicketType;

			$scope.activateUpdateButton = true;
			$scope.selectedButton = runType;
			$scope.runFlag = false;
			var statusParam = {};
			var previousBallStatus = "";

			switch(runType.toUpperCase()){
				case "WICKET" : $scope.lastBallStatus.push("WK"); 
								$scope.wicketFlag = true; 
								$scope.updateBatsmanStatus('out', null);
								$scope.updateTvScreen("WK," + wicketType + "," + $scope.getLastBatsman()); 
								break;

				case "WIDE"   : $scope.lastBallStatus.push("WB"); 
								++($scope.match.score); 
								$scope.wideBallFlag = true; 
								break;

				case "NO"     : $scope.lastBallStatus.push("NB"); 
								++($scope.match.score); 
								$scope.noBallFlag = true;
								break;

				case "SIX"    : $scope.lastBallStatus.push("6"); 
								$scope.match.score += 6; 
								$scope.updateTvScreen("SIX," + $scope.getRandomMessage('six') + "," + $scope.getCurrentBatsman()); 
								$scope.updateBatsmanStatus('run', '6');
								$scope.updateBatsmanScore = false;
								$scope.boundaryFlag = 6;
								break;

				case "FOUR"   : $scope.lastBallStatus.push("4"); 
								$scope.match.score += 4; 
								$scope.updateTvScreen("SIX," + $scope.getRandomMessage('four') + "," + $scope.getCurrentBatsman());
								$scope.updateBatsmanStatus('run', '4');
								$scope.updateBatsmanScore = false;
								$scope.boundaryFlag = 4;
								break;

				case "REVERT" : var lastBallStatus = ($scope.lastBallStatus.length) ? $scope.lastBallStatus : $scope.lastBallHistory[$scope.lastBallHistory.length - 1];
								var otherBallFlag = false;
								$scope.showLoader = true;
								if(!$scope.lastBallStatus.length) {
									$scope.previousBall.splice(($scope.previousBall.length - 1), 1);
								}

								angular.forEach(lastBallStatus, function(value, index){
									if ((value == 'WD') || (value == 'NB')){
										statusParam.runs = --$scope.match.runs;
										otherBallFlag = true;
									}

									if(value == 'WK') {
										if($scope.match.playing == $scope.match.battingTeam){
											statusParam.bowlingWickets = --($scope.match.battingWickets);
										} else {
											statusParam.bowlingWickets = --($scope.match.bowlingWickets);
										} 
										$scope.currentlyBatting({'name' : $scope.match.onStrike});
									}

									if(Number(value)) {
										statusParam.runs = $scope.match.runs = Number($scope.match.runs) - Number(value);	
									}
								});

								if($scope.match.playing == $scope.match.battingTeam){
									$scope.match.battingScore = $scope.match.runs;
									statusParam.runs = statusParam.battingScore = $scope.match.runs;
								} else {
									$scope.match.bowlingScore = $scope.match.runs;	
									statusParam.runs = statusParam.bowlingScore = $scope.match.runs;	
								}
								

								$scope.lastBallStatus = [];
								$scope.wideBallFlag = false;
								$scope.noBallFlag = false;
								$scope.wicketFlag = false;
								$scope.boundaryFlag = false;
								$scope.match.score = 0;
								$scope.updateBatsmanScore = true;

								statusParam = $scope.updateBatsman(statusParam);
								statusParam = $scope.updateSession(statusParam);

								dataReader.updateStatus({
									'data' : statusParam
								})
								.then(function(response){
									$scope.showLoader = false;
								})

								break;

				case "CANCEL" : $scope.lastBallStatus = [];
								$scope.wideBallFlag = false;
								$scope.noBallFlag = false;
								$scope.wicketFlag = false;
								$scope.boundaryFlag = false;
								$scope.match.score = 0;
								$scope.updateBatsmanScore = false;
								$scope.activateUpdateButton = false;
								break;	

				case "UPDATE" : $scope.activateUpdateButton = false;
								$scope.showLoader = true;	
								if( !($scope.wicketFlag || $scope.wideBallFlag || $scope.noBallFlag || $scope.boundaryFlag)) {
									$scope.runFlag = true;
									$scope.lastBallStatus.push(Number($scope.match.score)); 

									if(Number($scope.match.score) == 0){
										$scope.updateCurrentStaus("0,Dot Ball");
										$scope.updateBatsmanStatus('run', '0');
									}
								} else {
									if (!$scope.boundaryFlag && !$scope.wicketFlag && ($scope.wideBallFlag || $scope.noBallFlag)) {
										$scope.lastBallStatus.push(Number($scope.match.score) - 1);
									}
								}


								$scope.match.runs = Number($scope.match.runs) + Number($scope.match.score); 

								if($scope.match.playing == $scope.match.battingTeam){
									$scope.match.battingScore = $scope.match.runs;
									statusParam.runs = statusParam.battingScore = $scope.match.runs;
								} else {
									$scope.match.bowlingScore = $scope.match.runs;	
									statusParam.runs = statusParam.bowlingScore = $scope.match.runs;	
								}

								if($scope.wicketFlag) {
									if($scope.match.playing == $scope.match.battingTeam){
										statusParam.bowlingWickets = ++($scope.match.battingWickets);
									} else {
										statusParam.bowlingWickets = ++($scope.match.bowlingWickets);
									} 
								} 

								if( !($scope.wideBallFlag || $scope.noBallFlag) ){
									$scope.balls = parseFloat(parseFloat($scope.balls) + .1).toFixed(1);
									
									if($scope.match.playing == $scope.match.battingTeam){
										statusParam.battingOver = $scope.match.battingOver = $scope.balls;
									} else {
										statusParam.bowlingOver = $scope.match.bowlingOver = $scope.balls; 
									}

									if( !$scope.boundaryFlag ) {
										$scope.updateBatsmanStatus('run', Number($scope.match.score));
									}

								} else {
									if($scope.updateBatsmanScore && $scope.noBallFlag){
										$scope.updateBatsmanStatus('run', Number($scope.match.score) - 1);
									}
								}


								if( ( parseFloat($scope.balls - Math.floor($scope.balls)).toFixed(1) * 10) >= 6){
									if($scope.match.playing == $scope.match.battingTeam){
										$scope.balls = statusParam.battingOver = $scope.match.battingOver = parseFloat(parseFloat($scope.match.battingOver) + .4).toFixed(1) 
									} else {
										$scope.balls = statusParam.bowlingOver = $scope.match.bowlingOver = parseFloat(parseFloat($scope.match.bowlingOver) + .4).toFixed(1);
									}
								}

								

								angular.forEach($scope.lastBallStatus, function(value, index){
									if(previousBallStatus)
										previousBallStatus += "|";	

									previousBallStatus += value;
								});

								var tvMessage = "";
								angular.forEach($scope.lastBallStatus, function(value, index){
									switch(value){
										case "WD" : tvMessage = "WD," + tvMessage; break;
										case "NB" : tvMessage = "NB," + tvMessage; break;
										case "WK" : tvMessage += "WK," + $scope.getLastBatsman(); break;
										default: tvMessage += (value && value != "0") ? value + "," +  $scope.getCurrentBatsman() : "0,";
									}
								});

								console.log(tvMessage);
								
								$scope.previousBall.push(previousBallStatus);

								if($scope.previousBall.length > 6){
									$scope.previousBall.splice(0, 1);
								}
								
								$scope.lastBallHistory.push($scope.lastBallStatus);

								$scope.lastBallStatus = [];
								$scope.wideBallFlag = false;
								$scope.noBallFlag = false;
								$scope.wicketFlag = false;
								$scope.boundaryFlag = false;
								$scope.match.score = 0;
								$scope.updateBatsmanScore = true;
			

								statusParam.previousBall = $scope.match.previousBall = $scope.previousBall.join();
								
								statusParam = $scope.updateBatsman(statusParam);
								statusParam = $scope.updateSession(statusParam);

								statusParam = $scope.calculateRunRate(statusParam);

								dataReader.updateStatus({
									'data' : statusParam
								})
								.then(function(response){
									$scope.showLoader = false;
								})
								break;
			}
		}

		$scope.calculateRunRate = function(statusParam){
			var totalOver = ($scope.match.playing == $scope.match.battingTeam) ? $scope.match.battingOver : $scope.match.bowlingOver;
			var totalScore = ($scope.match.playing == $scope.match.battingTeam) ? $scope.match.battingScore : $scope.match.battingScore;
			
			var totalBalls = Math.floor(totalOver) * 6;

			var remainingBalls = Math.floor((totalOver - Math.floor(totalOver)) * 10);
			if (totalBalls >= 0){
				if (remainingBalls > 0){
					totalBalls += remainingBalls;
				} 
			}else {
				totalBalls = 0;
			}

			$scope.match.currentRunRate = statusParam.currentRunRate = parseFloat((totalScore * 6) / totalBalls).toFixed(2);

			return statusParam;
		}

		$scope.getLastBatsman = function(){
			var batsman = "";
			if($scope.lastBatsman.length){
				batsman = $scope.lastBatsman[$scope.lastBatsman.length - 1].name;
			}
			return batsman;
		}

		$scope.getCurrentBatsman = function(){
			var batsman = "";
			angular.forEach($scope.batsmanList, function(value, index){
				if(value.status == 'onStrike'){
					batsman = value.name;
				}
			})
			return batsman;
		}

		$scope.updateBatsmanStatus = function(status, statusValue){
			$scope.showLoader = true;
			angular.forEach($scope.batsmanList, function(value, index){
				if(value.status == 'onStrike'){
					if(status == 'out'){
						$scope.batsmanList[index].status = status;
						$scope.lastBatsman.push($scope.batsmanList[index]);
						if(index) {
							$scope.batsman2Ball = 0;
							$scope.batsman2Run = 0;
							$scope.batsman2 = "";
						} else {
							$scope.batsman1Ball = 0;
							$scope.batsman1Run = 0;
							$scope.batsman1 = "";
						}
					}

					if(status == 'run'){
						if(statusValue){
							$scope.batsmanList[index].run = Number($scope.batsmanList[index].run) + Number(statusValue);
							++$scope.batsmanList[index].ball;
							if(index) {
								$scope.batsman2Ball = $scope.batsmanList[index].ball;
								$scope.batsman2Run = $scope.batsmanList[index].run;
							} else {
								$scope.batsman1Ball = $scope.batsmanList[index].ball;
								$scope.batsman1Run = $scope.batsmanList[index].run;
							}
						}
					}
				}
			});
		}

		$scope.addBatsman = function(batsmanIndex, index) {
			$scope.showLoader = true;
			$scope.batsmanList[index] = ({
				'name' : $scope[batsmanIndex],
				'ball' : $scope[batsmanIndex + "Ball"],
				'run'  : $scope[batsmanIndex + "Run"],
				'status' : 'playing'
			});

			$scope.updateTvScreen("NBA," + $scope[batsmanIndex]); 
			$scope.updateBatsman();
		}

		$scope.updateBatsman = function(uploadParam) {
			var status = {};
			status['batsmanTwoStatus'] = $scope['batsman2'] + " " + $scope["batsman2Run"] + "(" + $scope["batsman2Ball"] + ")";
			status['batsmanOneStatus'] = $scope['batsman1'] + " " + $scope["batsman1Run"] + "(" + $scope["batsman1Ball"] + ")";
			
			if(typeof uploadParam == 'undefined') {
				dataReader.updateStatus({
					'data' : status
				})
				.then(function(response){
					$scope.showLoader = false;
				});
			} else {
				return angular.extend(uploadParam, status);
			}
		}

		$scope.addBowler = function(bowlerIndex) {
			$scope.showLoader = true;
			$scope.onBowling = $scope[bowlerIndex];
			$scope.bowlerList.push({
				'name' : $scope[bowlerIndex],
				'over' : 0,
				'run' : 0,
				'status' : 'bowling'
			});

			var currentStatus = $scope.match.currentStatus = "NBO," + $scope[bowlerIndex]; 
			dataReader.updateStatus({
				'data' : {
					'bowlerStatus' : $scope[bowlerIndex],
					'currentStatus': currentStatus
				}
			})
			.then(function(response){
				$scope.showLoader = false;
			});
		}

		$scope.currentlyBowling = function(bowler){
			$scope.showLoader = true;
			$scope.onBowling = bowler.name;
			dataReader.updateStatus({
				'data' : {
					'bowlerStatus' : bowler.name
				}
			})
			.then(function(response){
				$scope.showLoader = false;
			});

			
		}
			
		$scope.currentlyBatting = function(batsman){
			
			angular.forEach($scope.batsmanList, function(value, index){
				if(value.name.toUpperCase() == batsman.name.toUpperCase()){
					$scope.batsmanList[index].status = "onStrike";
				} else {
					$scope.batsmanList[index].status = "batting";
				}
			});

			if(batsman.status != 'out'){
				$scope.showLoader = true;
				$scope.match.onStrike = batsman.name;
				dataReader.updateStatus({
					'data' : {
						'onStrike' : batsman.name
					}
				})
				.then(function(response){
					$scope.showLoader = false;
				});
			}
		}

		$scope.updateTvStatus = function(status){
			$scope.tvButtonStatus = status;
		}

		$scope.notSorted = function(obj){
	        if (!obj) {
	            return [];
	        }
	        return Object.keys(obj);
	    }

	    $scope.start = function() {
	      cfpLoadingBar.start();
	    };

	    $scope.complete = function () {
	      cfpLoadingBar.complete();
	    }


		$scope.init();
		dataReader.getStatus()
				  .then($scope.initInning);
	}


	var cricControllers = angular.module('cricControllers', ['chieffancypants.loadingBar', 'ngAnimate'])
								.config(function(cfpLoadingBarProvider) {
								    cfpLoadingBarProvider.includeSpinner = true;
								})

        cricControllers
			.controller('MatchController', ['$scope', 'cfpLoadingBar', 'MATCH', 'KEYWORDS', 'DataReader', MatchController])
		    .controller('HomeController', ['$scope', 'cfpLoadingBar', '$location', 'MATCH', 'DataReader', HomeController]);
    