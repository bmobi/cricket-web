
	function HomeController($scope, cfpLoadingBar, $location, match, dataReader){
		$scope.secondTeam = $scope.firstTeam = [];  
		$scope.disableStartButton = true;

		$scope.teamList = {};
		$scope.match = match;
		$scope.previousMatchFlag = false;
		
		$scope.inning = {
			'team1': null,
			'team2': null,
			'batting' : null,
			'bowling' : null
		}

		function filterFromTeam(filterArray, filterValue){
			var otherTeam = [];
			angular.forEach(filterArray, function(value, index){
				if(value.name != filterValue) {
					otherTeam.push(filterArray[index]);
				}
			});

			return otherTeam;
		};

		$scope.getTeamLog = function(pocket){
			if($scope.inning[pocket]) {
				return $scope.teamList[$scope.inning[pocket]].logo;
			} else {
				return 'http://cricket.apphosthub.com/cric-app/public/images/teams/default.png'
			}
		}

		$scope.selectMatchType = function(type){
			$scope.match.overs = type;
		}

		$scope.changeSelection = function(selectedTeam, selectedFor) {
			if($scope.inning.team1 && $scope.inning.team2) {
				$scope.inning[selectedFor] = selectedTeam;
				$scope.inning[(selectedFor == 'batting') ? 'bowling' : 'batting'] = (selectedTeam == 'team1') ? 'team2' : 'team1';
				$scope.disableStartButton = false;
			} else {
				$scope.disableStartButton = true;
				alert("Please select a teams first");
			}
		};

		$scope.startMatch = function(){
			if(Number($scope.match.overs)) {
				$scope.match.battingTeam = $scope.inning[$scope.inning['batting']];
				$scope.match.bowlingTeam = $scope.inning[$scope.inning['bowling']];
				
				$scope.match.playing = $scope.match.battingTeam;
				dataReader.setStatus({
							'data' : $scope.match 
						})
						.then(function(response){
								$location.path('/match')
						});
			} else {
				alert("Please select match format (20 / 50/ test)");	
			}
		};

		$scope.$watch('inning.team1', function(newValue, oldValue){
			if(newValue) {
				$scope.secondTeam =  filterFromTeam($scope.allTeams, newValue);
			}
		});

		$scope.$watch('inning.team2', function(newValue, oldValue){
			if(newValue) {
				$scope.firstTeam = filterFromTeam($scope.allTeams, newValue);
			}
		});

		$scope.start = function() {
	      cfpLoadingBar.start();
	    };

	    $scope.complete = function () {
	      cfpLoadingBar.complete();
	    }

	    $scope.getPreviousMatches = function(){
	    	dataReader.getStatus()
	    		.then(function(response){
	    			$scope.previousMatchFlag = (response.matchStatus == 'playing') ? true : false;
	    		})
	    }

	    $scope.start();

		dataReader.getTeam()
				  .then(function(response){
					  	$scope.allTeams = response;
					  	angular.forEach($scope.allTeams, function(value, index){
					  		$scope.teamList[value.name] = value;
					  	});
						angular.copy($scope.allTeams, $scope.firstTeam); 
						angular.copy($scope.allTeams, $scope.secondTeam); 
						$scope.getPreviousMatches();
						$scope.complete();
				  })
	}


	// Match Page Controller

	function MatchController($scope, cfpLoadingBar, $window, match, keywords, dataReader){
		$scope = angular.extend($scope, keywords);

		$scope.ignoreStatusKeys = ['batsmanOneStatus', 'batsmanTwoStatus', 'battingOver', 
									'bowlingOver', 'battingTeam', 'bowlingTeam',
									'battingTeamImage', 'bowlingTeamImage', 'currentRunRate', 'favouriteTeam',
									'localDescription', 'matchStatus', 'onStrike', 'playing', 'previousBall', 
									'requiredRunRate', 'currentStatus'
							]

		$scope.getRandomMessage = function(type){
			return $scope.messages[type][Math.floor(Math.random() * $scope.messages[type].length)];
		}

		$scope.hasBeenClicked = function(value){
			if($scope.lastBallStatus.length == 0)
				return false;

			return ($scope.lastBallStatus.indexOf(value) >= 0);
		}

		$scope.batsmanList = [];
		$scope.init = function(){
			$scope.match = match;

			$scope.wideBallFlag = false;
			$scope.noBallFlag = false;
			$scope.wicketFlag = false;
			$scope.balls = 0.0;
			$scope.selectedButton = "";
			$scope.onStrike = "";
			$scope.onBowling = "";
			$scope.batsmanList = [];
			$scope.bowlerList = [];
			$scope.lastBatsman = [];

			$scope.temp = {};
			$scope.message = "";
			$scope.lastBallStatus = [];
			$scope.lastBallHistory = [];
			$scope.batsman2Run = 0;
			$scope.batsman2Ball = 0;
			$scope.batsman1Run = 0;
			$scope.batsman1Ball = 0;
			$scope.batsman1 = "";
			$scope.batsman2 = "";
			$scope.bowler1 = "";
			$scope.disableFinishButton = false;
			$scope.boundaryFlag = false;
			$scope.byeFlag = false;
			$scope.showLoader = false;
			$scope.lastMatchStatus = {};
			$scope.favOptionList = [];

		}

		$scope.finishInning = function(){
			$scope.showLoader = true;
			angular.copy($scope.match, $scope.temp); 

			$scope.temp.playing = ($scope.temp.battingTeam == $scope.temp.playing) ? $scope.temp.bowlingTeam : $scope.temp.battingTeam;
			$scope.temp.target  = $scope.match.target = ($scope.temp.battingTeam == $scope.temp.playing) ? $scope.temp.bowlingScore : $scope.temp.battingScore;
			$scope.temp.fallen  = $scope.temp.wickets;
			$scope.temp.runs   = $scope.temp.wickets = 0;
			$scope.temp.previousBall = 0;
			$scope.temp.onStrike = 0;
			$scope.temp.bowlerStatus = 0;
			$scope.temp.batsmanOneStatus = 0;
			$scope.temp.batsmanTwoStatus = 0;
			$scope.temp.ballsRemaining = 0;
			$scope.temp.currentStatus = {
		    	"NB" : "0",
		    	"WB" : "0",
		    	"RUN" : "0",
		    	"WK" : "0",
		    	"BYE" : "0",
		    	"LBYE" : "0",
		    	"MSG" : "Break",
		    	"OTHER" : "0"
		    };
		    $scope.temp.currentRunRate = "0";
		    $scope.temp = $scope.calculateRequiredRunRate($scope.temp);

		    angular.forEach(['score', 'wickets'], function(value, key){
		    	$scope.match[value] = "0";
			    if(typeof $scope.temp[value] != 'undefined')
			    	delete $scope.temp[value];
		   	});

			++($scope.temp.inning);
			if($scope.temp.inning >= 2){
				$scope.temp.matchStatus = 'finished';
			} else {
				$scope.temp.matchStatus = 'playing';
			}
			dataReader.setStatus({
					'data' : $scope.temp
				})
				.then(function(response){
					$scope.showLoader = false;
					if(response.inning <= 2){
						alert("First inning Has been finished");
						$scope.init();
						$scope.initInning(response);
					} else {
						++$scope.match.inning;
						$scope.disableFinishButton = true;
						$scope.message = "Match Finished";
						alert("Match Finished");
					}
				});
		}

		function stringToNumber(value){
			 return ( !isNaN(value) ) ? Number(value) : 0;
		}

		function updateLocalScope(){
			$scope.batsmanList = [];

			$scope.favOptionList.push({
				'id' : $scope.match.battingTeam,
				'value' : $scope.match.battingTeam
			});

			$scope.favOptionList.push({
				'id' : $scope.match.bowlingTeam,
				'value' : $scope.match.bowlingTeam
			});

			$scope.favOptionList.push({
				'id' : 'both',
				'value' : 'Both'
			});

			$scope.favOptionList.push({
				'id' : 'lagana',
				'value' : 'Lagana'
			});

			$scope.favTeam = $scope.match.favouriteTeam;

			$scope.match.runs = Number(($scope.match.playing == $scope.match.battingTeam) ? $scope.match.battingScore : $scope.match.bowlingScore);
			$scope.balls  = (($scope.match.playing == $scope.match.battingTeam) ? $scope.match.battingOver : $scope.match.bowlingOver);
			
			if($scope.match.inning > 2) {
				$scope.disableFinishButton = true;
				$scope.activateUpdateButton = false;
			}

			if($scope.match.target) {
				$scope.calculateRequiredRun();
				$scope.calculateRequiredRunRate($scope.match);
				$scope.modifiedScore = $scope.match.target;
				$scope.modifiedOvers = $scope.match.overs;
			}


			if($scope.match.batsmanOneStatus) {
				batsmanStatus = $scope.match.batsmanOneStatus.split(" ");
				$scope.batsman1 = ( batsmanStatus[0] == "0" || !batsmanStatus[0] ) ? "..." : batsmanStatus[0].replace("*", "");
				if(batsmanStatus.length == 2){
					runStatus = batsmanStatus[1].split("(");
					$scope.batsman1Run = Number(runStatus[0]);
					$scope.batsman1Ball = Number(runStatus[1].replace(")", ""));
				}

				if( !$scope.match.onStrike ){
					$scope.match.onStrike = batsmanStatus[0];
				}

				$scope.batsmanList.push({
					'name' : ( batsmanStatus[0] == "0" || !batsmanStatus[0] ) ? "..." : batsmanStatus[0].replace("*", ""),
					'run' : $scope.batsman1Run,
					'ball': $scope.batsman1Ball,
					'status': ($scope.match.onStrike.toUpperCase() == batsmanStatus[0].replace("*", "").toUpperCase()) ? 'onStrike' : 'playing'
				})
			}

			if($scope.match.batsmanTwoStatus) {
				batsmanStatus = $scope.match.batsmanTwoStatus.split(" ");
				$scope.batsman2 = ( batsmanStatus[0] == "0" || !batsmanStatus[0] ) ? "..." : batsmanStatus[0].replace("*", "");
				if(batsmanStatus.length == 2){
					runStatus = batsmanStatus[1].split("(");
					$scope.batsman2Run = Number(runStatus[0]);
					$scope.batsman2Ball = Number(runStatus[1].replace(")", ""));
				}

				if( !$scope.match.onStrike ){
					$scope.match.onStrike = batsmanStatus[0];
				}

				$scope.batsmanList.push({
					'name' : ( batsmanStatus[0] == "0" || !batsmanStatus[0] ) ? "..." : batsmanStatus[0].replace("*", ""),
					'run' : $scope.batsman2Run,
					'ball': $scope.batsman2Ball,
					'status': ($scope.match.onStrike.toUpperCase() == batsmanStatus[0].replace("*", "").toUpperCase()) ? 'onStrike' : 'playing'
				})
			}

			if($scope.match.bowlerStatus && $scope.match.bowlerStatus != "0") {
				$scope.bowlerList.push({
					'name' : $scope.match.bowlerStatus,
					'run' : 10,
					'ball': 10,
					'status': 'bowling'
				});
				$scope.onBowling = $scope.match.bowlerStatus;
				$scope.bowler1 = $scope.match.bowlerStatus;
			}

			$scope.lastBallStatus = [];
			$scope.previousBall = [];
			if($scope.match.previousBall && $scope.match.previousBall != "0") {
				$scope.previousBall = $scope.match.previousBall.split(",");
			}
		}

		$scope.initInning = function(response){

			var batsmanStatus = "";
			var runStatus = "";
			angular.forEach(response, function(value, index){
				$scope.match[index] = ($scope.ignoreStatusKeys.indexOf(index) == -1) ? stringToNumber(value) : value;
			});
			$scope.balls = 0.0;
			$scope.match.score = 0;
			updateLocalScope();
		}

		$scope.updateDLscore = function(){
			if(confirm("You are about to change the score, Do you want to proceed?")) {
				var params = {};
				params.target = $scope.match.target = $scope.modifiedScore;
				params.overs = $scope.match.overs = $scope.modifiedOvers;

				$scope.calculateRequiredRun();
				$scope.calculateRequiredRunRate($scope.match);
				
				dataReader.updateStatus({
					'data' : params
				})
				.then(function(response){
					$scope.showLoader = false;
				})
			}
		}			  


		$scope.updateFavourite = function(){
			$scope.showLoader = true;
			var param = {};

			param['favouriteTeam'] = $scope.match.favouriteTeam = $scope.favTeam;

			dataReader.updateStatus({
					'data' : param
			})
			.then(function(response){
				$scope.showLoader = false;
			})
		}	  

		$scope.substract = function(type){
			var modifieldValue = {};
			$scope.showLoader = true;
			if(type == 'score') {
				type = ($scope.match.playing == $scope.match.battingTeam) ? 'battingScore' : 'bowlingScore';
				$scope.match[type]--;
				modifieldValue[($scope.match.playing == $scope.match.battingTeam) ? 'battingScore' : 'bowlingScore'] = $scope.match[type];
			}

			if(type == 'wickets') {
				type = ($scope.match.playing == $scope.match.battingTeam) ? 'battingWickets' : 'bowlingWickets';

				if(Number($scope.match[type]) <= 0) {
					$scope.showLoader = false;
					return;
				} 

				$scope.match[type]--;
				modifieldValue[type] = $scope.match[type];
			}

			if(type == 'balls'){
				type = ($scope.match.playing == $scope.match.battingTeam) ? 'battingOver' : 'bowlingOver';
				if(Number($scope.match[type]) <= 0){
					$scope.balls = $scope.match[type] = 0.0;
					modifieldValue[type] = $scope.match[type];
				} else {
					if( (($scope.match[type] - Math.floor($scope.match[type]).toFixed(1)) * 10) == 0){
						$scope.balls = $scope.match[type] = parseFloat(parseFloat($scope.match[type]) - 0.5).toFixed(1);
					} else {
						$scope.balls = $scope.match[type] = parseFloat(parseFloat($scope.match[type]) - 0.1).toFixed(1);
					}

					if ($scope.balls == 0 || isNaN($scope.balls) ){
						$scope.balls = $scope.match[type] = 0.0;
					}
					modifieldValue[type] = $scope.match[type];
				}
			}
			$scope.updateTeamStatus(modifieldValue);
			
		}

		$scope.add = function(type) {
			var modifieldValue = {};
			$scope.showLoader = true;
			if(type == 'score') {
				type = ($scope.match.playing == $scope.match.battingTeam) ? 'battingScore' : 'bowlingScore';
				$scope.match[type]++;
				modifieldValue[type] = $scope.match[type];
			}

			if(type == 'wickets') {
				type = ($scope.match.playing == $scope.match.battingTeam) ? 'battingWickets' : 'bowlingWickets';

				if(Number($scope.match[type]) >= 10) {
					$scope.showLoader = false;
					return;
				}
				
				$scope.match[type]++;
				modifieldValue[type] = $scope.match[type];
			}

			if(type == 'balls'){
				type = ($scope.match.playing == $scope.match.battingTeam) ? 'battingOver' : 'bowlingOver';
				
				var availableBalls = parseFloat(parseFloat($scope.match[type]) + 0.1).toFixed(1);
				if( (($scope.match[type] - Math.floor($scope.match[type]).toFixed(1)) * 10) >= 6){
					availableBalls = parseFloat(parseFloat($scope.match[type]) + 0.4).toFixed(1);
				}

				$scope.balls = $scope.match[type] = (isNaN(availableBalls)) ? 0 : availableBalls;
				modifieldValue[type] = $scope.match[type];
			}

			$scope.updateTeamStatus(modifieldValue);
			
		}

		$scope.updateTeamStatus = function(modifieldValue){
			$scope.showLoader = true;
			modifieldValue = $scope.updateSession(modifieldValue);
			dataReader.updateStatus({
				'data' : modifieldValue
			})
			.then(function(response){
				$scope.showLoader = false;
			})
		}

		$scope.updateMarketRate = function() {

			$scope.showLoader = true;
			dataReader.updateStatus({
				'data' : {
					'markerRateOne' : $scope.match.markerRateOne,
					'markerRateTwo' : $scope.match.markerRateTwo
				}
			})
			.then(function(response){
				$scope.showLoader = false;
			})
		}

		$scope.updateSession = function(uploadParam){
			if($scope.match.sessionOver && $scope.match.sessionOver != "0"){
				$scope.showLoader = true;
				var currentOver = (match.playing == match.battingTeam) ? match.battingOver : match.bowlingOver;
				var currentRun = (match.playing == match.battingTeam) ? match.battingScore : match.bowlingScore;

				var overRequired = Math.floor($scope.match.sessionOver) - Math.floor(currentOver);

				var totalBalls = Math.floor(overRequired) * 6;

				var remainingBalls = Math.round( (parseFloat(currentOver).toFixed(1) - Math.floor(currentOver)) * 10 );
				if (totalBalls >= 0){
					if (remainingBalls > 0){
						totalBalls -= remainingBalls;
					} 
				}else {
					totalBalls = 0;
				}

				$scope.match.runXBallTwo = (totalBalls < 0) ? 0 : totalBalls;

				var runRequired = Number($scope.match.sessionStatusTwo) - Number(currentRun);
				if(runRequired > 0) {
					$scope.match.runXBallOne = runRequired;
				} else {
					$scope.match.runXBallOne = 0;
				}

				if(typeof uploadParam == 'undefined') {
					dataReader.updateStatus({
						'data' : {
							'sessionOver' 		: $scope.match.sessionOver,
							'sessionStatusOne' 	: $scope.match.sessionStatusOne,
							'sessionStatusTwo' 	: $scope.match.sessionStatusTwo,
							'runXBallOne' 		: $scope.match.runXBallOne,
							'runXBallTwo' 		: $scope.match.runXBallTwo
						}
					})
					.then(function(response){
						$scope.showLoader = false;
					});
				} else {
					return angular.extend(uploadParam, {
						'sessionOver' 		: $scope.match.sessionOver,
						'sessionStatusOne' 	: $scope.match.sessionStatusOne,
						'sessionStatusTwo' 	: $scope.match.sessionStatusTwo,
						'runXBallOne' 		: $scope.match.runXBallOne,
						'runXBallTwo' 		: $scope.match.runXBallTwo
					});
				}
			}

			return uploadParam;
		}

		$scope.updateRunXBall = function(){
			$scope.showLoader = true;
			dataReader.updateStatus({
				'data' : {
					'runXBallOne' : $scope.match.runXBallOne,
					'runXBallTwo' : $scope.match.runXBallTwo
				}
			})
			.then(function(response){
				$scope.showLoader = false;
			})
		}

		$scope.calculateRunXBall = function(){

			$scope.updateRunXBall();
		}

		

		$scope.updateLastBallStatus = function(){
			$scope.previousBall.splice(0, 1);
			$scope.match.previousBall = $scope.previousBall.join();
			/*dataReader.updateStatus({
				'data' : {
					'previousBalls' : $scope.match.previousBalls 
				}
			});*/
		}

		$scope.updateLiveMessage = function(message){
			$scope.match.localDescription = message;
			dataReader.updateStatus({
				'data' : {
					'localDescription' : message
				}
			});
		}

		function validBatsmanName(batsman){
			return (batsman && batsman != "...");
		}

		$scope.addRun = function(run){
			if( !(validBatsmanName($scope.batsman1) && validBatsmanName($scope.batsman2) ) ){
				alert("Please enter batsman name");
				return;
			}

			$scope.activateUpdateButton = true;
			$scope.lastBallStatus.push(run);
			$scope.match.score = parseInt($scope.match.score) + parseInt(run);
			$scope.updateBatsmanScore = true;
		}

		$scope.updateMatchStatus = function(matchKey){
			var updatedStatus = {};
			
			if(matchKey == 'balls') {
				matchKey = (match.playing == match.battingTeam) ? "battingOver" : "bowlingOver";
				$scope.match[matchKey] = $scope.balls;
			}

			if((matchKey == 'battingScore') || (matchKey == 'bowlingScore')){
				$scope.match.runs = $scope.match[matchKey];
			}
			
			updatedStatus[matchKey] = $scope.match[matchKey];
			
			statusParam = $scope.updateSession(updatedStatus);
			statusParam = $scope.calculateRunRate(updatedStatus);

			dataReader.updateStatus({
				'data' : updatedStatus
			});
		}

		function convertOverToBall(overs){
				var totalBalls = Math.floor(overs) * 6;

				var remainingBalls = Math.round( (parseFloat(overs).toFixed(1) - Math.floor(overs)) * 10 );
				if (totalBalls >= 0){
					if (remainingBalls > 0){
						totalBalls += remainingBalls;
					} 
				}else {
					totalBalls = 0;
				}	

				return Number(totalBalls);		
		}

		function convertBallToOver(balls){
				var totalOver = Math.floor(balls / 6);

				var remainingBalls = balls - (totalOver * 6);

				if(remainingBalls){
					return parseFloat(totalOver + "." + remainingBalls).toFixed(1);
				} else {
					return parseFloat(totalOver).toFixed(1);
				}
		}


		$scope.updateRun = function(runType, wicketType){
			if ($scope.match.inning > 2){
				$scope.activateUpdateButton = false;
				alert("Match has been finished, Please reset Team");
				return;
			}

			if((!$scope.wicketFlag) && !(validBatsmanName($scope.batsman1) && validBatsmanName($scope.batsman2) ) ){
				alert("Please enter batsman name");
				return;
			}

			wicketType = (typeof wicketType == 'undefined') ? "" : wicketType;

			$scope.activateUpdateButton = true;
			$scope.selectedButton = runType;
			$scope.runFlag = false;
			var statusParam = {};
			var previousBallStatus = "";

			switch(runType.toUpperCase()){
				case "WICKET" : $scope.lastBallStatus.push("WK"); 
								$scope.wicketFlag = true; 
								$scope.updateBatsmanStatus('out', null);
								break;

				case "WIDE"   : $scope.lastBallStatus.push("WB"); 
								++($scope.match.score); 
								$scope.wideBallFlag = true; 
								break;

				case "NO"     : $scope.lastBallStatus.push("NB"); 
								++($scope.match.score); 
								$scope.noBallFlag = true;
								break;

				case "SIX"    : $scope.lastBallStatus.push("6"); 
								$scope.match.score += 6; 
								$scope.updateBatsmanStatus('run', '6');
								$scope.updateBatsmanScore = false;
								$scope.boundaryFlag = 6;
								break;

				case "FOUR"   : $scope.lastBallStatus.push("4"); 
								$scope.match.score += 4; 
								$scope.updateBatsmanStatus('run', '4');
								$scope.updateBatsmanScore = false;
								$scope.boundaryFlag = 4;
								break;

				case "BYE"	  : $scope.lastBallStatus.push("BY"); 
								$scope.updateBatsmanScore = false;
								$scope.byeFlag = true;
								break;

				case "LBY"	  : $scope.lastBallStatus.push("LBY"); 
								$scope.updateBatsmanScore = false;
								$scope.lbyeFlag = true;
								break;

				case "REVERT" : $scope.activateUpdateButton = false;
								statusParam = {};
								
								angular.forEach($scope.lastMatchStatus, function(value, index){
									$scope.match[index] = statusParam[index] = ($scope.ignoreStatusKeys.indexOf(index) == -1) ? stringToNumber(value) : value;
								});

								statusParam['currentStatus'] = {
									"WB" : "0",
									"NB" : "0",
									"WK" : "0",
									"BYE" : "0",
							    	"LBYE" : "0",	
									"RUN" : "0",
									"MSG" : "0",
									"OTHER" : "0"
								}

								updateLocalScope();
								

								$scope.lastBallStatus = [];
								$scope.wideBallFlag = false;
								$scope.noBallFlag = false;
								$scope.wicketFlag = false;
								$scope.boundaryFlag = false;
								$scope.match.score = 0;
								$scope.updateBatsmanScore = true;
								$scope.byeFlag = false;
								$scope.lbyeFlag = false;
								$scope.lastMatchStatus = {};

								$scope.calculateRequiredRun();	
								statusParam = $scope.updateBatsman(statusParam);
								statusParam = $scope.updateSession(statusParam);

								statusParam = $scope.calculateRunRate(statusParam);
								statusParam = $scope.calculateRequiredRunRate(statusParam);
								//statusParam = $scope.calculateRemainingBall(statusParam);

								dataReader.updateStatus({
									'data' : statusParam
								})
								.then(function(response){
									$scope.showLoader = false;
								})

								break;

				case "CANCEL" : $scope.lastBallStatus = [];
								$scope.wideBallFlag = false;
								$scope.noBallFlag = false;
								$scope.wicketFlag = false;
								$scope.boundaryFlag = false;
								$scope.match.score = 0;
								$scope.updateBatsmanScore = false;
								$scope.activateUpdateButton = false;
								$scope.byeFlag = false;
								$scope.lbyeFlag = false;
								break;	

				case "UPDATE" : $scope.activateUpdateButton = false;
								$scope.showLoader = true;	
								angular.copy($scope.match, $scope.lastMatchStatus);
								
								if( !($scope.wicketFlag || $scope.wideBallFlag || $scope.noBallFlag) ) {
									$scope.runFlag = true;
									
									if(Number($scope.match.score) == 0){
										$scope.updateBatsmanStatus('run', '0');
									}
								} else {
									if (!$scope.wicketFlag && ($scope.wideBallFlag || $scope.noBallFlag)) {
										$scope.lastBallStatus.push(Number($scope.match.score) - 1);
									}
								}


								$scope.match.runs = Number($scope.match.runs) + Number($scope.match.score); 

								if($scope.match.playing == $scope.match.battingTeam){
									$scope.match.battingScore = $scope.match.runs;
									statusParam.runs = statusParam.battingScore = $scope.match.runs;
								} else {
									$scope.match.bowlingScore = $scope.match.runs;	
									statusParam.runs = statusParam.bowlingScore = $scope.match.runs;	
								}

								if($scope.wicketFlag) {
									if($scope.match.playing == $scope.match.battingTeam){
										statusParam.battingWickets = ++($scope.match.battingWickets);
									} else {
										statusParam.bowlingWickets = ++($scope.match.bowlingWickets);
									} 
								} 

								if( !($scope.wideBallFlag || $scope.noBallFlag) ){
									$scope.balls = parseFloat(parseFloat($scope.balls) + .1).toFixed(1);
									
									if($scope.match.playing == $scope.match.battingTeam){
										statusParam.battingOver = $scope.match.battingOver = $scope.balls;
									} else {
										statusParam.bowlingOver = $scope.match.bowlingOver = $scope.balls; 
									}

									if($scope.updateBatsmanScore && !$scope.byeFlag) {
										$scope.updateBatsmanStatus('run', Number($scope.match.score));
									}

								} else {
									if($scope.updateBatsmanScore && $scope.noBallFlag && !$scope.byeFlag){
										$scope.updateBatsmanStatus('run', Number($scope.match.score) - 1);
									}
								}


								if( ( parseFloat($scope.balls - Math.floor($scope.balls)).toFixed(1) * 10) >= 6){
									if($scope.match.playing == $scope.match.battingTeam){
										$scope.balls = statusParam.battingOver = $scope.match.battingOver = parseFloat(parseFloat($scope.match.battingOver) + .4).toFixed(1) 
									} else {
										$scope.balls = statusParam.bowlingOver = $scope.match.bowlingOver = parseFloat(parseFloat($scope.match.bowlingOver) + .4).toFixed(1);
									}
								}

								
								var counter = 1;
								var batsmanRun = 0;
								angular.forEach($scope.lastBallStatus, function(value, index){
									if( !isNaN(value) ){
										batsmanRun = Number(value);
									}

									if(counter < 3) {
										if(previousBallStatus) {
											if(value){
												previousBallStatus += "|";	
												previousBallStatus += value;
											}
										} else {
											previousBallStatus += value;
										}
									}
									counter++;
								});


								if(batsmanRun % 2){
									changeBatsmanStrike();
								}

								if(!previousBallStatus) {
									previousBallStatus = "0";
								}

								$scope.previousBall.push(previousBallStatus);

								if($scope.previousBall.length > 6){
									$scope.previousBall.splice(0, 1);
								}
								
								$scope.lastBallHistory.push($scope.lastBallStatus);

								$scope.match.currentStatus = statusParam.currentStatus = {
									"WB" : ($scope.wideBallFlag) ? "1" : "0",
									"NB" : ($scope.noBallFlag) ? "1" : "0",
									"WK" : ($scope.wicketFlag) ? "1" : "0",
									"RUN" : (isNaN(batsmanRun)) ? 0 : batsmanRun,
									"LBYE" : ($scope.lbyeFlag) ? "1" : "0",
									"BYE" : ($scope.byeFlag) ? "1" : "0",
									"MSG" : "0",
									"OTHER" : "0"
								}

								$scope.calculateRequiredRun();


								$scope.lastBallStatus = [];
								$scope.wideBallFlag = false;
								$scope.noBallFlag = false;
								$scope.wicketFlag = false;
								$scope.boundaryFlag = false;
								$scope.match.score = 0;
								$scope.updateBatsmanScore = true;
								$scope.lbyeFlag = false;
								$scope.byeFlag = false;
			

								statusParam.previousBall = $scope.match.previousBall = $scope.previousBall.join();
								
								statusParam = $scope.updateBatsman(statusParam);
								statusParam = $scope.updateSession(statusParam);

								statusParam = $scope.calculateRunRate(statusParam);
								statusParam = $scope.calculateRequiredRunRate(statusParam);
								statusParam = $scope.calculateRemainingBall(statusParam);

								dataReader.updateStatus({
									'data' : statusParam
								})
								.then(function(response){
									$scope.showLoader = false;

									if($scope.match.target){
										if(Number($scope.match.target) < Number($scope.match.runs)){

										}

										if(Number($scope.match.target) == Number($scope.match.runs)){

										}

										
									}

								})
								break;
			}
		}

		$scope.resetTvStatus = function() {

			$scope.match.currentStatus = {
				"WB" : "0",
				"NB" : "0",
				"WK" : "0",
				"BYE" : "0",
		    	"LBYE" : "0",	
				"RUN" : "0",
				"MSG" : "0",
				"OTHER" : "0"
			}

			dataReader.updateStatus({
				'data' : {
					'currentStatus' : $scope.match.currentStatus
				}
			})
			.then(function(response){
				$scope.match.currentStatus.MSG = "";
				$scope.match.currentStatus.OTHER = "";
				$scope.tvButtonStatus = "";
				$scope.showLoader = false;
			})
		}

		$scope.uploadTvStatus = function(){
			
			dataReader.updateStatus({
				'data' : {
					'currentStatus' : $scope.match.currentStatus
				}
			})
			.then(function(response){
				$scope.match.currentStatus.MSG = "";
				$scope.match.currentStatus.OTHER = "";
				$scope.tvButtonStatus = "";
				$scope.showLoader = false;
			})
		}

		$scope.updateTvStatus = function(status, action, otherType){
			
			$scope.tvButtonStatus = status;
			if(typeof $scope.match.currentStatus.MSG == 'undefined') {
				$scope.match.currentStatus = {
					"WB" : "0",
					"NB" : "0",
					"WK" : "0",
					"BYE" : "0",
					"LBYE" : "0",	
					"RUN" : ( !isNaN($scope.match.score) ) ? Number($scope.match.score) : "0",
					"MSG" : "0",
					"OTHER" : "0"
				}
			}

			if(otherType){
				$scope.match.currentStatus = {
					"WB" : "0",
					"NB" : "0",
					"WK" : "0",
					"BYE" : "0",
					"LBYE" : "0",	
					"RUN" : "0",
					"MSG" : "0",
					"OTHER" : action
				}
			} else {
				if( !(validBatsmanName($scope.batsman1) && validBatsmanName($scope.batsman2) ) ){
					$scope.tvButtonStatus = "";
					alert("Please enter batsman name");
					return;
				}

				$scope.match.currentStatus.MSG = action;
			}
			$scope.uploadTvStatus();
		}

		$scope.calculateRemainingBall = function(statusParam){
			//if($scope.match.target){
				var totalOver = ($scope.match.playing == $scope.match.battingTeam) ? $scope.match.battingOver : $scope.match.bowlingOver;
				
				var totalBalls = Math.floor(totalOver) * 6;
				var ballsRemaining = Math.floor((totalOver - Math.floor(totalOver)) * 10);
				if (totalBalls >= 0){
					if (ballsRemaining > 0){
						totalBalls += ballsRemaining;
					} 
				}else {
					totalBalls = 0;
				}

				$scope.match.currentBalls = statusParam.currentBalls = totalBalls;

				totalBalls = (Number($scope.match.overs) * 6) - totalBalls;
				$scope.match.ballsRemaining = statusParam.ballsRemaining = totalBalls;
			//}
			return statusParam;
		}

		$scope.calculateRequiredRun = function(){
			if($scope.match.target){
				var totalScore = ($scope.match.playing == $scope.match.battingTeam) ? $scope.match.battingScore : $scope.match.bowlingScore;
				$scope.requiredRun = Number($scope.match.target) - Number(totalScore);
				return ($scope.requiredRun > 0) ? $scope.requiredRun : 0;
			}

			return "--";

		}

		$scope.calculateRequiredRunRate = function(statusParam){
			if($scope.match.target){
				var totalOver = ($scope.match.playing == $scope.match.battingTeam) ? $scope.match.battingOver : $scope.match.bowlingOver;
				var totalScore = ($scope.match.playing == $scope.match.battingTeam) ? $scope.match.battingScore : $scope.match.bowlingScore;
			
				var totalBalls = Math.floor(totalOver) * 6;
				var remainingBalls = Math.floor((totalOver - Math.floor(totalOver)) * 10);
				if (totalBalls >= 0){
					if (remainingBalls > 0){
						totalBalls += remainingBalls;
					} 
				}else {
					totalBalls = 0;
				}

				totalBalls = (Number($scope.match.overs) * 6) - totalBalls;

				statusParam.requiredRunRate = parseFloat(((Number($scope.match.target) - totalScore) / totalBalls) * 6).toFixed(2);
				$scope.match.requiredRunRate = statusParam.requiredRunRate = ( !isNaN(statusParam.requiredRunRate) ) ? statusParam.requiredRunRate : 0;
			}

			return statusParam;
		}


		$scope.calculateRunRate = function(statusParam){
			var totalOver = ($scope.match.playing == $scope.match.battingTeam) ? $scope.match.battingOver : $scope.match.bowlingOver;
			var totalScore = ($scope.match.playing == $scope.match.battingTeam) ? $scope.match.battingScore : $scope.match.bowlingScore;
			
			var totalBalls = Math.floor(totalOver) * 6;

			var remainingBalls = Math.floor((totalOver - Math.floor(totalOver)) * 10);
			if (totalBalls >= 0){
				if (remainingBalls > 0){
					totalBalls += remainingBalls;
				} 
			}else {
				totalBalls = 0;
			}

			var runRate = parseFloat((totalScore * 6) / totalBalls).toFixed(2);
			$scope.match.currentRunRate = statusParam.currentRunRate = ( isNaN(runRate) ) ? 0 : runRate;

			return statusParam;
		}

		$scope.getLastBatsman = function(){
			var batsman = "";
			if($scope.lastBatsman.length){
				batsman = $scope.lastBatsman[$scope.lastBatsman.length - 1].name;
			}
			return batsman;
		}

		$scope.getCurrentBatsman = function(){
			var batsman = "";
			angular.forEach($scope.batsmanList, function(value, index){
				if(value.status == 'onStrike'){
					batsman = value.name;
				}
			})
			return batsman;
		}

		$scope.updateBatsmanStatus = function(status, statusValue){
			$scope.showLoader = true;
			angular.forEach($scope.batsmanList, function(value, index){
				if(value.status == 'onStrike'){
					if(status == 'out'){
						$scope.batsmanList[index].status = status;
						$scope.lastBatsman.push($scope.batsmanList[index]);
						if(index) {
							$scope.batsman2Ball = 0;
							$scope.batsman2Run = 0;
							$scope.batsman2 = "...";
						} else {
							$scope.batsman1Ball = 0;
							$scope.batsman1Run = 0;
							$scope.batsman1 = "...";
						}
					}

					if(status == 'run'){
						if(statusValue){
							$scope.batsmanList[index].run = Number($scope.batsmanList[index].run) + Number(statusValue);
							++$scope.batsmanList[index].ball;
							if(index) {
								$scope.batsman2Ball = $scope.batsmanList[index].ball;
								$scope.batsman2Run = $scope.batsmanList[index].run;
							} else {
								$scope.batsman1Ball = $scope.batsmanList[index].ball;
								$scope.batsman1Run = $scope.batsmanList[index].run;
							}
						}
					}
				}
			});
		}

		$scope.addBatsman = function(batsmanIndex, index) {
			if ($scope.match.inning > 2){
				$scope.activateUpdateButton = false;
				alert("Match has been finished, Please reset Team");
				return;
			}

			$scope.showLoader = true;
			$scope.batsmanList[index] = ({
				'name' : $scope[batsmanIndex],
				'ball' : $scope[batsmanIndex + "Ball"],
				'run'  : $scope[batsmanIndex + "Run"],
				'status' : 'playing'
			});

			$scope.currentlyBatting($scope.batsmanList[index]);
			//$scope.updateBatsman();
		}

		$scope.updateBatsman = function(uploadParam) {
			if ($scope.match.inning > 2){
				$scope.activateUpdateButton = false;
				alert("Match has been finished, Please reset Team");
				return;
			}

			var status = {};
			status['batsmanTwoStatus'] = $scope.match.batsmanTwoStatus = $scope['batsman2'] + (($scope.getCurrentBatsman() == $scope['batsman2']) ? "* " : " ") + $scope["batsman2Run"] + "(" + $scope["batsman2Ball"] + ")";
			status['batsmanOneStatus'] = $scope.match.batsmanOneStatus = $scope['batsman1'] + (($scope.getCurrentBatsman() == $scope['batsman1']) ? "* " : " ") + $scope["batsman1Run"] + "(" + $scope["batsman1Ball"] + ")";
			
			if(typeof uploadParam == 'undefined') {
				dataReader.updateStatus({
					'data' : status
				})
				.then(function(response){
					$scope.showLoader = false;
				});
			} else {
				return angular.extend(uploadParam, status);
			}
		}

		$scope.addBowler = function(bowlerIndex) {
			$scope.showLoader = true;
			$scope.onBowling = $scope[bowlerIndex];
			$scope.bowlerList.push({
				'name' : $scope[bowlerIndex],
				'over' : 0,
				'run' : 0,
				'status' : 'bowling'
			});

			dataReader.updateStatus({
				'data' : {
					'bowlerStatus' : $scope[bowlerIndex]
				}
			})
			.then(function(response){
				$scope.showLoader = false;
			});
		}

		$scope.currentlyBowling = function(bowler){
			$scope.showLoader = true;
			$scope.onBowling = bowler.name;
			dataReader.updateStatus({
				'data' : {
					'bowlerStatus' : bowler.name
				}
			})
			.then(function(response){
				$scope.showLoader = false;
			});

			
		}

		function changeBatsmanStrike(){
			var batsmanOnStrike = "";
			angular.forEach($scope.batsmanList, function(value, index){
				if($scope.batsmanList[index].status != "onStrike"){
					batsmanOnStrike = $scope.batsmanList[index];
				}
			});
			$scope.currentlyBatting(batsmanOnStrike);
		}
			
		$scope.currentlyBatting = function(batsman){
			
			angular.forEach($scope.batsmanList, function(value, index){
				if(value.name.toUpperCase() == batsman.name.toUpperCase()){
					$scope.batsmanList[index].status = "onStrike";
				} else {
					$scope.batsmanList[index].status = "batting";
				}
			});

			if(batsman.status != 'out'){
				$scope.showLoader = true;
				$scope.match.onStrike = batsman.name;

				var status = {};
				if(validBatsmanName($scope.batsman2)){
					status['batsmanTwoStatus'] = $scope.match.batsmanTwoStatus = $scope['batsman2'] + (($scope.getCurrentBatsman() == $scope['batsman2']) ? "* " : " ") + $scope["batsman2Run"] + "(" + $scope["batsman2Ball"] + ")";
				}

				if(validBatsmanName($scope.batsman1)){
					status['batsmanOneStatus'] = $scope.match.batsmanOneStatus = $scope['batsman1'] + (($scope.getCurrentBatsman() == $scope['batsman1']) ? "* " : " ") + $scope["batsman1Run"] + "(" + $scope["batsman1Ball"] + ")";
				}

				status['onStrike'] = batsman.name;
				dataReader.updateStatus({
					'data' : status
				})
				.then(function(response){
					$scope.showLoader = false;
				});
			}
		}

		
		$scope.notSorted = function(obj){
	        if (!obj) {
	            return [];
	        }
	        return Object.keys(obj);
	    }

	    $scope.start = function() {
	      cfpLoadingBar.start();
	    };

	    $scope.complete = function () {
	      cfpLoadingBar.complete();
	    }

	    $scope.gotoTeamSelection = function(){
	    	$window.location.href = '#/team';
	    }

	    $scope.isValidTarget = function(){
	    	return (Number($scope.match.target) && !isNaN($scope.match.target));
	    }

	    $scope.getTeamLog = function(teamName){
	    	var defaultUrl = $scope.teamIconPath + 'default.png';
	    	if(teamName && teamName != "0") {
	    		var imageUrl = $scope.teamIconPath + teamName.toLowerCase() + '.png';
	    		
	    	 	var http = new XMLHttpRequest();

			    http.open('HEAD', imageUrl, false);
			    http.send();

			    return (http.status != 404) ? imageUrl : defaultUrl;
			}

			return defaultUrl;
	    }


		$scope.init();
		dataReader.getStatus()
				  .then($scope.initInning);
	}


	var cricControllers = angular.module('cricControllers', ['chieffancypants.loadingBar', 'ngAnimate'])
								.config(function(cfpLoadingBarProvider) {
								    cfpLoadingBarProvider.includeSpinner = true;
								})

        cricControllers
			.controller('MatchController', ['$scope', 'cfpLoadingBar', '$window', 'MATCH', 'KEYWORDS', 'DataReader', MatchController])
		    .controller('HomeController', ['$scope', 'cfpLoadingBar', '$location', 'MATCH', 'DataReader', HomeController]);
    