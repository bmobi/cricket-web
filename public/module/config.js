function config($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise("/home");
    $stateProvider
        .state('logout',{
            url : "/logout",
            templateUrl : "public/page/logout.html",
            data: {
                requireLogin: true
            }
        })
        .state('login',{
            url : "/login",
            templateUrl : "public/page/login.html",
            data: {
                requireLogin: false
            }
        })
        .state('home',{
            url : "/home",
            templateUrl : "public/page/home.html",
            data: {
                requireLogin: true
            }
        })
        .state('match',{
            url : "/match",
            templateUrl : "public/page/dashboard.html",
            data: {
                requireLogin: true
            }
        })
}

angular
    .module('cricApp')
    .config(["$stateProvider", "$urlRouterProvider", config])
    .run(['$rootScope', '$state', function($rootScope, $state){

    }]);
   