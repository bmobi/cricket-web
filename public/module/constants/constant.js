'use strict';
/* App Module */
var cricConstants = angular.module('cricConstants', [])

cricConstants

    .constant('MATCH', (function() {
    	return {
		    "ballsRemaining" : "0",
		    "batsmanOneStatus" : "0",
		    "batsmanTwoStatus" : "0",
		    "battingOver" : "0",
		    "battingScore" : "0",
		    "battingTeam" : "0",
		    "battingTeamImage" : "0",
		    "battingWickets" : "0",
		    "bowlerStatus" : "0",
		    "bowlingOver" : "0",
		    "bowlingScore" : "0",
		    "bowlingTeam" : "0",
		    "bowlingTeamImage" : "0",
		    "bowlingWickets" : "0",
		    "currentStatus" : {
		    	"NB" : "0",
		    	"WB" : "0",
		    	"RUN" : "0",
		    	"WK" : "0",
		    	"BYE" : "0",
		    	"LBYE" : "0",
		    	"MSG" : "Teams are ready",
		    	"OTHER": "0"
		    },
		    "favouriteTeam" : "0",
		    "markerRateOne" : "0",
		    "markerRateTwo" : "0",
		    "previousBall" : "0",
		    "currentBalls" : "0",
		    "currentRunRate" : "0",
		    "requiredRunRate" : "0",
		    "runXBallOne" : "0",
		    "runXBallTwo" : "0",
		    "runs" : "0",
		    "sessionOver" : "0",
		    "sessionStatusOne" : "0",
		    "sessionStatusTwo" : "0",
		    "target" : "0",
		    "overs" : "0",
		    "inning" : 1,
		    "matchStatus" : 'playing',
		    "localDescription" : "This app is under development."
		}
    })())
    .constant('KEYWORDS', (function(){
    	return{
	    	'messages' : {
				'four' : ['Lovely', 'Beauty', 'Genius', 'Bullet', 'Chicky', 'Clever'],
				'six'  : ['Lovely', 'Messive', 'Beauty', 'Genius', 'Maximum', 'Huge', 'Chicky', 'Clever'],
				'other' : ['Easy', 'Hard', 'Clever']
			},
			'availableRuns' : [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
			'availableActions' : {
				'bowled': 'Bowled',
				'runOut' : 'Run Out',
				'catch' : 'Catch Out',
				'stump' : 'Stump',
				'lbw'	: 'LBW',
				'hit'	: 'Hit Wicket',
				'retired' : 'Retired Hurt',
				'timeout' : 'Time Out'
			},
			'availableOthers' : {
				'inAir' : 'In air',
				'ball'	: 'Ball',
				'3rd'	: '3rd Umpire',
				'teaBreak' : 'Tea Break',
				'dl' : 'D/L - Score',
				'drawn' : 'Match drawn',
				'rain' : 'Raining',
				'noResult' : 'No Result',
				'newBatsman' : 'New Batsman'
			},
			'teamIconPath' : 'http://cricket.apphosthub.com/cric-app/public/images/teams/'
			//'teamIconPath' : 'http://localhost/cricket-app/public/images/teams/'
		}
    })())
    .constant('MATCH-BKP', (function() {
    	return {
		    "ballsRemaining" : 5,
		    "batsmanOneStatus" : "Bradman 25(10)",
		    "batsmanTwoStatus" : "Sachin 25(15)",
		    "battingOver" : 16.3,
		    "battingScore" : 144,
		    "battingTeam" : "KKR",
		    "battingTeamImage" : "",
		    "battingWickets" : 1,
		    "bowlerStatus" : "Asish Nehra",
		    "bowlingOver" : 0,
		    "bowlingScore" : 0,
		    "bowlingTeam" : "SRH",
		    "bowlingTeamImage" : "",
		    "bowlingWickets" : 0,
		    "currentStatus" : "4,Lovely",
		    "favouriteTeam" : "",
		    "markerRateOne" : "80",
		    "markerRateTwo" : "82",
		    "previousBall" : "2,2,W,6,2,6",
		    "requiredRunRate" : 0,
		    "runXBallOne" : "0",
		    "runXBallTwo" : "0",
		    "runs" : 0,
		    "sessionOver" : "0",
		    "sessionStatusOne" : "278",
		    "sessionStatusTwo" : "0",
		    "target" : 176,
		    "teamOne" : "KKR",
		    "teamTwo" : "SRH",
		    "inning" : 1
		}
    })());	