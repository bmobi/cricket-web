    function RequestHandler($http, $q){
        return{
            prepareRequest      : prepareRequest,
            preparePostRequest  : preparePostRequest,
            prepareGetRequest   : prepareGetRequest,
            prepareImageRequest : prepareImageRequest,
            prepareAttachmentRequest: prepareAttachmentRequest,
            prepareJsonRequest  : prepareJsonRequest
        }

        var postParam = {
            method  : 'POST',
            url     : '',
            data    : '',
            headers : {'Content-Type': 'application/x-www-form-urlencoded'},
            transformRequest : transformTicketRequest
        }

        var getParam = {
            method  : 'GET',
            url     : '',
        }


        function prepareRequest(method, param){
            var requestParam = (method.toUpperCase() == 'POST') ? $.extend({}, postParam, param) : $.extend({}, getParam, param);

            if(method.toUpperCase() == 'POST'){
                requestParam.data = (requestParam.data) ? $.param(requestParam.data) : "";
            }

            return $http(requestParam)
                .then(sendResponseData )
                .catch(sendResponseError);
        }

        function preparePostRequest($param) {
            $param.data = ($param.data) ? $.param($param.data) : "";
            return $http({
                method  : 'POST',
                url     : $param.url,
                data    : $param.data,
                headers : {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'},
                transformRequest : angular.identity
            })
                .then(sendResponseData )
                .catch(sendResponseError )
        }


        function prepareImageRequest($param) {
            $param.data = ($param.data) ? $.param($param.data) : "";
            $param.url  =  $param.url + '?'+ $param.data;
            return $http({
                method: 'GET',
                url     : $param.url,
                dataType: 'binary',
                responseType: 'arraybuffer',
                headers: {'Content-Type':'image/png','X-Requested-With':'XMLHttpRequest'},
                processData: false,
                transformRequest: angular.identity
            })
                .then(sendResponseData )
                .catch(sendResponseError )
        }

        function prepareAttachmentRequest($param) {
            return $http({
                method  : 'POST',
                url     : $param.url,
                data    : $param.data,
                processData: false,
                contentType: false,
                headers:  {'Content-Type': undefined},
                transformRequest: angular.identity
            })
                .then(sendResponseData )
                .catch(sendResponseError )
        }

        function prepareJsonRequest($param){
            return $http.jsonp($param.url).
            success(function(status) {
                console.log(status);
            }).
            error(function(status) {
                console.log(status);
            });


        }


        function prepareGetRequest($param){
            return $http({
                method  : 'GET',
                url     : $param.url,
                headers: {
                    'Content-Type': 'json',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': 'GET'
                }
            })
                .then(sendResponseData )
                .catch(sendResponseError )

        }


        function transformTicketRequest(data, headersGetter) {return data;}
        function transformEmailAttachmentsRequest(data, headersGetter) {
            var formData = new FormData();
            //need to convert our json object to a string version of json otherwise
            // the browser will do a 'toString()' on the object which will result
            // in the value '[Object object]' on the server.
            formData.append("email-param", angular.toJson(data['email-param']));
            //now add all of the assigned files

            formData.append("file", encodeURIComponent(data['email-param'].attachment));

            return formData;
        }
        function sendResponseData(response) { return response.data;}
        function sendResponseError(response) {

            //return $q.reject('Error retrieving book(s). (HTTP status: ' + response.status + ')');
        }

    };


    function DataReader(requestHandler){
        return {
            'getTeam' : getTeam,
            'getStatus' : getStatus,
            'setStatus'  : setStatus,
            'updateStatus' : updateStatus
        }

        function getStatus(){
            return requestHandler.prepareGetRequest({'url' : 'getStatus'});
        }

        function setStatus($params){
            $params = angular.extend({'url' : 'setStatus'}, $params);
            return requestHandler.preparePostRequest($params);
        }


        function getTeam(){
            return requestHandler.prepareGetRequest({'url' : 'getTeam'});
        }

        function updateStatus($params){
            $params = angular.extend({'url' : 'updateStatus'}, $params);
            return requestHandler.preparePostRequest($params);
        }
    }

    function UserService(requestHandler){
        return{
            'isLogin' : isLogin
        };

        function isLogin(){
            return requestHandler.prepareGetRequest({'url' : 'isLogin'});
        }
    }

    var cricServices = angular.module('cricServices', [])

    cricServices
         .factory('RequestHandler', ['$http', '$q', RequestHandler])
         .factory('DataReader', ['RequestHandler', DataReader]);
         
