'use strict';
/* App Module */

if(typeof String.prototype.trim !== 'function') {
  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, ''); 
  }
}

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

(function () {
    angular.module('cricApp', [
        'ui.router',
        'ngRoute',
        'ngResource',
        'ngCookies',
        'ngMessages',
        'angularUtils.directives.dirPagination',
        'angular-loading-bar',
        'cricConstants',
        'cricFilters',
        'cricServices',
        'cricDirectives',
        'cricControllers'
    ]);
})();