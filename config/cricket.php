<?php
return [
	     /*
          Api Configs
          */
    'api' => [
        'base_uri' => 'https://rest.cricketapi.com/rest/v2/',
        'access_key' => '8b4e979095a77b847184770a1b473810',
        'secret_key' => '1d820ec67aba6d37d715081a7572ce15',
        'app_id' => 'livecricketscore',
        'device_id' => 'yfxfni5fa63smkdwd23h413',
    ],


     /*
      Api Urls
      */
    'urls' => [
        'auth' => [
            'callUri' => 'auth',
            'url' => 'auth',
            'fileName' => 'access_token.txt',
            'getFunctionName' => 'get_valid_token',
            'putFunctionName' => false,
        ],
        'recent_matches' => [
            'callUri' => 'schedule',
            'url' => 'recent_matches',
            'fileName' => 'recent_matches.txt',
            'getFunctionName' => 'get_recent_matches',
            'putFunctionName' => 'put_recent_matches',
        ],
        'upcoming_matches' => [
            'callUri' => false,
            'url' => false,
            'fileName' => 'recent_matches.txt',
            'getFunctionName' => 'get_upcoming_matches',
            'putFunctionName' => false,
        ],
        'live_matches' => [
            'callUri' => 'recent_matches',
            'url' => 'live_matches',
            'fileName' => 'live_matches.txt',
            'getFunctionName' => 'get_live_matches',
            'putFunctionName' => 'put_live_matches',
        ]
    ],
];